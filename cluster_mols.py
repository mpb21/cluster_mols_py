#!/usr/bin/env python3

#quickly cluster molecules in an input sdf file and display them in pymool
#Matthew Baumgartner and David Koes


#REQUIREMENTS - I hope this is all of them.
#babel - openbabel.org with python bindings
#fastcluster - for clustering
#scipy
#numpy



import sys
import os
import subprocess as sp
import tempfile
import re
import pickle as pickle
import itertools
import multiprocessing as mp

from mttkinter import mtTkinter as tkinter
import tkinter.simpledialog
import tkinter.messagebox
import tkinter.filedialog
import Pmw
import tkinter
from tkinter import *

from pymol import cmd
    
from threading import Thread
from time import sleep
import time
import urllib.request, urllib.error, urllib.parse
import shutil
import gzip
import socket
import tarfile
import json


try:
    from openbabel import openbabel as ob #3.0
    from openbabel import pybel as pybel
except ImportError:
    sys.stderr.write("\n\n'openbabel' package not found! Install from openbabel.org (Make sure to compile python bindings)\n\n")
    sys.exit()

try:
    import numpy as np #There have been problems that may occur if your numpy installation is older than 0.15, but I haven't confirmed it
except ImportError:
    sys.stderr.write("\n\n'numpy' package not found! Install using 'sudo pip install numpy'\n\n")
    sys.exit()
try:
    import scipy
except ImportError:
    sys.stderr.write("\n\n'scipy' package not found! Install using 'sudo pip install scipy'\n\n")
    sys.exit()


import scipy.cluster
try:
    import fastcluster
except ImportError:
    sys.stderr.write('\n\nError: "fastcluster" package missing! Run "sudo easy_install fastcluster" to install.\n\n')


    

#Amount of output to print out - higher is more (default = 2)
# Values above 3 can make pymol unstable for some reason
#this can be set through the command line with the '-v' option
DEBUG = 2

#force recomputing everything - i.e. ignore pickled data (Default = False)
REDO = False

#Do a runtime profile of the script
PROFILE = False
if PROFILE:
    import cProfile
    import pstats


#executable for babel. Hopefully it can be just 'babel', but if not try to figure out the path a bit
BABEL = None

POSSIBLE_BABEL_LOCS = ['obabel', '/usr/bin/obabel', '/opt/local/bin/obabel', '/usr/local/bin/obabel']

#make sure you can reach babel
for bab in POSSIBLE_BABEL_LOCS:
    try:
        p = sp.Popen([bab, '--help'], stdout = sp.PIPE, stderr = sp.PIPE)
        out,err = p.communicate()
        if DEBUG > 2:
            print('Found babel at', bab)
        BABEL = bab
        break
    except OSError:
        #since you can't find it, you are probably on OSX and you are having hard to pin down PATH issues. So try to figure out where it is with an absolute path
        if DEBUG > 2:
            print("Can't find babel at:", bab)
        

if BABEL == None:
    sys.stdout.write("\n\nError! Can not find the 'obabel' command line tool! It must be in your $PATH.\n")
    sys.stdout.write('If you do not have openbabel, get it from here:\n')
    sys.stdout.write('www.openbabel.org\n\n')
    sys.exit()


#Boolean for whether the similarities are being calculated now or not
global RUNNING
RUNNING = False

#Boolean that you can set during the runtime to kill a currently running simulation
#set by typing the following into the pymol command line: cluster_mols. KILL = True
#2-17-14 This may or may not work.
KILL = False

#boolean for whether to show clustering dendrogram
SHOW_PLOT = False

if SHOW_PLOT:
    try:
        import matplotlib.pyplot as plt
    except:
        print("unable to import matplotlib, to generate a plot you need to have it installed")
        SHOW_PLOT = False
    
#Boolean for whether to automaticially switch to the Cluster Compounds page after calculating the similarities
SWITCH_PAGE_ON_CALC_SIMS = True

HIGHLIGHT_BUTTONS = False

if HIGHLIGHT_BUTTONS:
    HIGHLIGHT_COLOR = 'green'
    HIGHLIGHT_COLOR_ACTIVE = '#66FF66'
else:
    HIGHLIGHT_COLOR = '#d9d9d9'
    HIGHLIGHT_COLOR_ACTIVE = '#ececec'
    
#Version string
VERSION = 'v2.1'

#Release date - MM-DD-YYYY
RELEASE_DATE = '04-27-2022' 

#boolean for whether to autorun the calculations after choosing a ligand
AUTORUN_CLUSTERING = True

####################################################################################
##################  objectfocus: Keyboard navigation through the clusters ##########
##################          And one-button compound selection             ##########
####################################################################################
#moved objectfocus from it's own file into here to make installation easier

 
UP, DOWN = -1, 1
in_focus = -1

#help function
def objectfocus_help():
    doc = '''
DESCRIPTION
    
    Remap some keyboard keys to make navigating pymol by keyboard easier. In particular, 
    navigating the clusters objects produced by the cluster_mols package. 
    
    It also allows for marking compounds for later analysis through a stack like object.

USAGE
    There are no command line options. This function is just a wrapper for this help.
    
MAPPED KEYS:
    Enabled Object Manipulation:
        'CTRL-A','ALT-A' -- Decrease frame
        'CTRL-D','ALT-D' -- Increase frame
        'CTRL-W','ALT-W','PgUp' -- Move up to the next object
        'CTRL-S','ALT-S','PgDn' -- Move down to the next object
    
    Selected Compound List Manipulation:
        'F1' -- Print currently selected molecule
        'F2' -- Remove most recently added compound
        'F3' -- Add currently visible compound to list
        'F4','F12' -- Print List
        
    Compound Information:
        'CTRL-F','ATL-F' -- Check for available vendors
    '''
    print(doc)
cmd.extend('objectfocus', objectfocus_help)

def get_names():
    global in_focus
    
    names = cmd.get_names_of_type("object:group")
    names = [n for n in names if re.search(r'cluster_((\d+)|(singletons))$', n)]
    if len(names) == 0: #fallback on everything
        names = cmd.get_names("public_objects")
    return names

def get_prefix():
    #get all of the cluster names
    names = get_names()
    
    #get the prefix of the first one
    pref = re.search('(.*)_cluster_.*', names[in_focus]).group(1) #probably going to need a try except here
    
    return pref
    
def update_list():
    '''
    From the list of selected molecules, print the names
    '''
    selected_objs = get_prefix() + '_selected'
    selected = []

    for i in range(1, cmd.count_states(selected_objs) + 1):
        selected.append(cmd.get_model(selected_objs, i).molecule.title)
    
    return selected

def object_focus(direction):
    """
    A simple script that remaps the PgUp and PgDn keys to walk through
    objects in the object list.
    dkoes - limit to iterating over groups with the name *cluster_%d
    """
 
    global in_focus
 
    names = get_names()
    
    if in_focus >= 0 and in_focus < len(names): #disable previous
        cmd.disable(names[in_focus])
    in_focus += direction

    if in_focus < 0:
        in_focus = 0
    if in_focus > len(names) - 1:
        in_focus = len(names) - 1
    
    cur_obj = names[in_focus]
    cmd.enable(cur_obj)
    cmd.enable(cur_obj + '_*')
    cmd.frame(1)

def get_cur_obj():
    ''' Get the model of the currently displayed molecule  '''
    names = get_names()

    global in_focus
    if in_focus < 0:
        in_focus = 0
    if in_focus > len(names) - 1:
        in_focus = len(names) - 1
    cur_obj = names[in_focus]
    
    #if show_contacts was used, the ligands will be in cur_obj + "_ligands", if not, it will just be cur_obj
    if cur_obj + "_ligands" in cmd.get_names():
        return cur_obj + "_ligands"
    else:
        return cur_obj

def get_title():
    '''Get title of currently displayed molecule (as determined by the
    global in_focus variable that is set with pgup/pgdown'''
    cur_obj = get_cur_obj()
    m = cmd.get_model(cur_obj,cmd.get_frame())

    return  m.molecule.title
        
def display_title():
    '''Print title of current mol'''
    print(get_title())      

def add_current(selected):
    '''Add the currently displaced molecule's name to selected; print the set'''
    cur = get_title()
    print('current title:', cur)
    if cur not in selected:
        
        #pymol object of selected molecules
        selected_objs = get_prefix() + '_selected'
        
        #extract it to an object
        extract_mol(selected_objs, selected)
        
        selected = update_list()
    
    print(' '.join(selected))
    
def extract_mol(selected_objs, selected):
    ''' Copy the currently selected object from the cluster object into the specified object '''
    cur = get_cur_obj()

    target_frame = 1
    
    if selected_objs in cmd.get_names():
        target_frame = cmd.count_states(selected_objs) + 1
        
    cmd.create(name = selected_objs, selection = cur, source_state = cmd.get_frame(), target_state = target_frame, discrete = 1, zoom = 0)
    #for some reason with discrete = 1 the last object dissapears until you show it again. 
    cmd.show('sticks', selected_objs)
    cmd.disable(selected_objs)
    
def remove_current(selected):
    '''Remove the currently displayed molecule's name from selected; print the set'''
    
    #TODO: Make this a little more flexible in figuring out which selected object to delete from
    #pymol object of selected molecules
    selected_objs = get_prefix() + '_selected'
    
    if selected_objs not in cmd.get_names('objects', enabled_only = 1):
        print(selected_objs, 'must be enabled to delete molecules from it')
    else:
        delete_mol(selected_objs, cmd.get_frame())
        selected = update_list()
    
    print(' '.join(selected))
    
def delete_mol(selected_objs, frame):
    ''' Remove the currently displayed molecule from the selected molecule object '''
    
    #temp object to copy molecules to 
    tmp_obj = 'tmp_selected_foo_asdksladcs'
    
    #list of all frames in the selected objects
    all_frames = list(range(1, cmd.count_states(selected_objs) + 1))
    
    if frame not in all_frames:
        print('Warning! Attempting to delete invalid frame', frame, 'from', selected_objs, '. Doing Nothing.')
    else:
        #remove it from the list of frames to keep
        all_frames.remove(frame)
        
        for dest_frame,i in enumerate(all_frames):
            dest_frame += 1 #shift to pymol indexing
            cmd.create(tmp_obj, selected_objs, i, dest_frame, discrete = 1, zoom = 0)
        
        cmd.delete(selected_objs)
        cmd.set_name(tmp_obj, selected_objs)
        #for some reason with discrete = 1 the last object dissapears until you show it again. 
        cmd.show('sticks', selected_objs)

def print_selected():
    selected = update_list()
    print('\n'.join(selected)+'\n')
    
def clear_selected(selected):
    ''' We don't ever want to call this function as it will lose user's work '''
    del selected[:]
    
    #pymol object of selected molecules
    selected_objs = get_prefix() + '_selected'
    #delete the selected molecules
    cmd.delete(selected_objs)
    
def goleft():
    cmd.frame(cmd.get_frame() - 1)

def goright():
    cmd.frame(cmd.get_frame() + 1)

def _molport_vendor_check(molport_id):
    ''' 
    TODO: make this run remotely. bs4 is not preinstalled
    
    Scrape the molport website to see if compounds are available and if so,
    how much they cost.
    
    Arguments:
    molport_id -- MolPort compound id (ex. MolPort-004-997-157)
    
    Returns:
    vendor_info -- list of vendors if any and their prices
    '''
    print('Looking for vendors of:', molport_id)
    url = 'https://www.molport.com/shop/molecule-link/' + str(molport_id)
    
    try:
        from bs4 import BeautifulSoup
    except ImportError:
        print('Error! Cannot find python package "bs4"!')
        return
    data  = urllib.request.urlopen(url).read()
    
    parsed_html = BeautifulSoup(data)
    
    
    image = parsed_html.find(attrs = {'class':'image'})
    availability = image.text.strip()
    
    print("Availability:", availability)
    
    if availability not in ['On request']:
        
        divs = parsed_html.find_all('div')
        
        #get the supplier info in a roundabout way. This can be optimized.
        suppliers = []
        
        for div in divs:
            if div.get('class') == ['verse']:
                suppliers.append(div)
        
        supplier = suppliers[1]
        supplier.find_all(attrs = {'class':'suppheading'})
        
        #pull out the suppliers
        sups = supplier.find_all(attrs = {'class' : 'bg1'})
        
        supplier_names = []
        
        for sup in sups:
            supplier_name = sup.find(attrs = {'class' : 'suppheading'}).text
            supplier_names.append(supplier_name)
            
            if DEBUG > 3:
                print('supplier_name:', supplier_name)
            
            #get the shipping and handling info
            sh_table = sup.find(attrs = {'class' : 'tableleft'})
            us_sh = re.search('S&H to USA: ([.\d]+) USD', sh_table.text.replace('\n', ' ')).group(1)
            
            min_order = ''
            if 'Minimum order' in sh_table.text.replace('\n', ' '):
                min_order = re.search('Minimum order: ([.\d]+) USD', sh_table.text.replace('\n', ' ')).group(1)
            
            #get the lists of amounts of compounds and prices
            amounts = [ i.text.replace('\t', '').replace('\r\n', '') for i in supplier.find_all(attrs = {'class':'align-left'}) ]
            prices = [ i.text for i in  supplier.find_all(attrs = {'class':'pricecolor'}) ]
            
            if DEBUG > 3:
                print('amounts:', amounts)
                print('prices: ', prices)
            
            assert len(amounts) == len(prices)
            
            #see if there is a listing for 5 mg, if so report that. If not, just report the first one
            try:
                idx = amounts.index('5 mg')
            except ValueError:
                idx = 0
            
            #grab the amounts and prices
            amt = amounts[idx]
            price = prices[idx]
            
            print(supplier_name)
            print('    ', amt, '      ', price)
            print('    ', 'S&H to USA:', us_sh, 'USD')
            if min_order:
                print('    ', 'Minimum order:', min_order, 'USD')
        
def _zinc_vendor_check(zinc_id):
    ''' 
    TODO: make this run remotely. bs4 is not preinstalled
    
    Query the zinc website for availablity of the zinc id
    This is really hacky and should be replaced. 
    TODO: Add support for MolPort compounds
    
    Arguments:
    zinc_id -- numeric portion of the zinc id
    
    Returns:
    vendors -- list of vendors
     '''
    
    url = 'http://zinc.docking.org/substance/{id}'.format(id = zinc_id)
    
    #scrape the webpage
    data = urllib.request.urlopen(url).read()
    
    #really hacky way of getting the vendors
    
    #pull out the lines corresponding to the vendors. The annotations are similar so we have to parse this way
    keep_lines = []
    for line in data.split('\n'):
        if keep_lines:
            if line.startswith('</div>'):
                break
            keep_lines.append(line)
        if line.startswith('<h3 class="purchasable catalogs report-title" id="vendors">Vendors</h3>'):
            keep_lines.append(line)
            
    #now grab the vendor names
    #line looks like: <li title="Catalog Mcule version 2013-08-14" '
    vendors = []
    for line in keep_lines:
        if line.strip().startswith('<li title="Catalog'):
            vendor = re.search('<li title="Catalog (.*) version.*', line.strip()).group(1)
            vendors.append(vendor)
            
    #if one of the vendors is molport,  search for the price
    if 'Molport' in vendors:
        #get the molport id from the url
        for line in keep_lines:
            if line.strip().startswith('<li><a href="http://www.molport.com/buy-chemicals/molecule-link/'):
                molport_id = re.search('<li><a href="http://www.molport.com/buy-chemicals/molecule-link/(.*)"', line).group(1)
    
                _molport_vendor_check(molport_id)
    
    return vendors

def check_for_vendors():
    ''' Check Zinc to see if there are any vendors listed for the currently selected molecule '''
    
    title = get_title()
    
    vendor_check(title)
    
    
def vendor_check(title):
    ''' Check to see if there are vendors for the compound'''
    
    if title.startswith('ZINC'):
        #assumes the title is something like: 'ZINC16947153;-8.82197'
        #TODO: make this more flexible
        zinc_id = int(title.split(';')[0].strip()[4:])
        
        vendors = _zinc_vendor_check(zinc_id)
        
        print(title.split(';')[0], 'has', len(vendors), 'listed')
        print('Vendors:', vendors)
    
    elif title.startswith('MolPort'):
        #Assumes the title is something like: 'MolPort-006-780-373;5;-6.9334574;-6.93346'
        molport_id = title.split(';')[0]
        
        vendors = _molport_vendor_check(molport_id)
        
    else:
        print('Error! Molecule title must start with a ZINC or Molport ID.')
    

cmd.extend('vendor_check', vendor_check)

selected_set = []

#object focus keyboard controls

cmd.set_key('CTRL-A', goleft)
cmd.set_key('ALT-A', goleft)
cmd.set_key('CTRL-D', goright)
cmd.set_key('ALT-D', goright)
cmd.set_key('CTRL-W', object_focus, [UP])
cmd.set_key('ALT-W', object_focus, [UP])
cmd.set_key('CTRL-S', object_focus, [DOWN])
cmd.set_key('ALT-S', object_focus, [DOWN])

cmd.set_key('PgUp', object_focus, [UP])
cmd.set_key('PgDn', object_focus, [DOWN])

cmd.set_key('F1', display_title)
cmd.set_key('F2', remove_current, [selected_set])
cmd.set_key('F3', add_current, [selected_set])
#cmd.set_key('F6', clear_selected, [selected_set]) #we never actually want this to happen
cmd.set_key('F4', print_selected)
cmd.set_key('F12', print_selected)

cmd.set_key('CTRL-F', check_for_vendors)
cmd.set_key('ALT-F', check_for_vendors)




def sdfsize(sdffile):
    '''
    Count the number of compounds in a sdf file.
    
    Arguments:
    sdffile -- sdf file name
    
    Returns:
    num -- number of compounds
    '''
    if os.path.splitext(sdffile)[1] == '.sdf':
        num = int(sp.Popen(['grep', '-c', '\$\$\$\$', sdffile], stdout = sp.PIPE).communicate()[0].strip())
        if DEBUG > 2:
            print(num, sdffile)
    elif os.path.splitext(sdffile)[1] == '.gz':
        num = int(sp.Popen(['zgrep', '-c', '\$\$\$\$', sdffile], stdout = sp.PIPE).communicate()[0].strip())
        if DEBUG > 2:
            print(num, sdffile)
    else:
        raise IOError(str(sdffile) + ' not a sdf\n')
    return num

def not_dollars(line):
    ''' 
    Check to see if the input line is the end of the molecule.
    
    Arguments:
    line -- line from sdf file
    
    Returns:
    boolean
    '''
    
    return "$$$$" != line.strip('\n')

def _my_area(model, chain, resi, name):
    #calculate solvent accessible surface area
    cmd.set('dot_solvent', 1)
    
    dot_density = 2
    old_dot_density = cmd.get('dot_density')
    
    if dot_density != old_dot_density:
        cmd.set('dot_density', dot_density)
    
    area = cmd.get_area('{model} and c. {chain} and i. {resi} and name {name}'.format(model = model, chain = chain, resi = resi, name = name), quiet = 1)
    
    #set it back
    if dot_density != old_dot_density:
        cmd.set('dot_density', old_dot_density)
    
    if DEBUG > 4:
        print(model, chain, resi, name, area)
    
    return area

def _is_unpaired_atom(polar_atoms, mdl, ch, resid, nm):
    ''' Given atom discriptors, and some other atoms, see if it is unpaired 
    If it is unpaired, return a selection string
    
    '''
    
    atom_sele = '{model} and c. {chain} and i. {resi} and n. {name}'.format(model = mdl, chain = ch, resi = resid, name = nm)
    
    #avg_dist = cmd.distance(atom_sele, polar_atoms, mode = 2)
    
    myspace = {'distances': [],
               'cmd' : cmd,
               'atom_sele' : atom_sele,
               'c':'c'
               }
    #.format(model = 'model', chain = 'chan', resi = 'resi', name = 'name')
    #cmd.iterate(polar_atoms, "distances.append(cmd.distance(atom_sele, polar_atoms))", space = myspace)
    
    temp_name = 'asdcanlsdcuntsaladsecn'
    avg_dist = cmd.distance(temp_name, atom_sele, polar_atoms, mode = 2)
    if DEBUG > 4:
        print(mdl, ch, resid, nm, avg_dist)
    
    #if avg_dist == 0, it means that there are no polar contacts, i.e. it is unpaired
    if avg_dist == 0.0:
        return atom_sele
    else: #if it is paired, return None
        return None

def show_contacts(selection,selection2,result="contacts",cutoff=3.6, bigcutoff = 4.0, SC_DEBUG = DEBUG):
    """
    USAGE
    
    show_contacts selection, selection2, [result=contacts],[cutoff=3.2],[bigcutoff=3.5]
    
    Show various polar contacts, the good, the bad, and the ugly.
    
    Edit MPB 6-26-14: The distances are heavy atom distances, so I upped the default cutoff to 4.0
    
    Returns:
    True/False -  if False, something went wrong
    """
    if SC_DEBUG > 4:
        print('Starting show_contacts')
        print('selection = "' + selection + '"')
        print('selection2 = "' + selection2 + '"')
    
    starttime = time.time() 
        

    #if the group of contacts already exist, delete them
    cmd.delete(result)

    # ensure only N and O atoms are in the selection
    all_don_acc1 = selection + " and (donor or acceptor)"
    all_don_acc2 = selection2 + " and  (donor or acceptor)"
    
    if SC_DEBUG > 4:
        print('all_don_acc1 = "' + all_don_acc1 + '"')
        print('all_don_acc2 = "' + all_don_acc2 + '"')
    
    #if theses selections turn out not to have any atoms in them, pymol throws crytic errors when calling the dist function like:
    #'Selector-Error: Invalid selection name'
    #So for each one, manually perform the selection and then pass the reference to the distance command and at the end, clean up the selections
    #the return values are the count of the number of atoms
    all1_sele_count = cmd.select('all_don_acc1_sele', all_don_acc1)
    all2_sele_count = cmd.select('all_don_acc2_sele', all_don_acc2)
    
    #print out some warnings
    if DEBUG > 3:
        if not all1_sele_count:
            print('Warning: all_don_acc1 selection empty!')
        if not all2_sele_count:
            print('Warning: all_don_acc2 selection empty!')
    
    ########################################
    allres = result + "_all"
    if all1_sele_count and all2_sele_count:
        cmd.distance(allres, 'all_don_acc1_sele', 'all_don_acc2_sele', bigcutoff, mode = 0)
        cmd.set("dash_radius", "0.05", allres)
        cmd.set("dash_color", "purple", allres)
        cmd.hide("labels", allres)
    
    ########################################
    #compute good polar interactions according to pymol
    polres = result + "_polar"
    if all1_sele_count and all2_sele_count:
        cmd.distance(polres, 'all_don_acc1_sele', 'all_don_acc2_sele', cutoff, mode = 2) #hopefully this checks angles? Yes
        cmd.set("dash_radius","0.126",polres)
    
    ########################################
    #When running distance in mode=2, the cutoff parameter is ignored if set higher then the default of 3.6
    #so set it to the passed in cutoff and change it back when you are done.
    old_h_bond_cutoff_center = cmd.get('h_bond_cutoff_center') # ideal geometry
    old_h_bond_cutoff_edge = cmd.get('h_bond_cutoff_edge') # minimally acceptable geometry
    cmd.set('h_bond_cutoff_center', bigcutoff)
    cmd.set('h_bond_cutoff_edge', bigcutoff)
        
    #compute possibly suboptimal polar interactions using the user specified distance
    pol_ok_res = result + "_polar_ok"
    if all1_sele_count and all2_sele_count:
        cmd.distance(pol_ok_res, 'all_don_acc1_sele', 'all_don_acc2_sele', bigcutoff, mode = 2) 
        cmd.set("dash_radius", "0.06", pol_ok_res)

    #now reset the h_bond cutoffs
    cmd.set('h_bond_cutoff_center', old_h_bond_cutoff_center)
    cmd.set('h_bond_cutoff_edge', old_h_bond_cutoff_edge) 
    
    
    ########################################
    
    onlyacceptors1 = selection + " and (acceptor and !donor)"
    onlyacceptors2 = selection2 + " and (acceptor and !donor)"
    onlydonors1 = selection + " and (!acceptor and donor)"
    onlydonors2 = selection2 + " and (!acceptor and donor)"  
    
    #perform the selections
    onlyacceptors1_sele_count = cmd.select('onlyacceptors1_sele', onlyacceptors1)
    onlyacceptors2_sele_count = cmd.select('onlyacceptors2_sele', onlyacceptors2)
    onlydonors1_sele_count = cmd.select('onlydonors1_sele', onlydonors1)
    onlydonors2_sele_count = cmd.select('onlydonors2_sele', onlydonors2)    
    
    #print out some warnings
    if SC_DEBUG > 2:
        if not onlyacceptors1_sele_count:
            print('Warning: onlyacceptors1 selection empty!')
        if not onlyacceptors2_sele_count:
            print('Warning: onlyacceptors2 selection empty!')
        if not onlydonors1_sele_count:
            print('Warning: onlydonors1 selection empty!')
        if not onlydonors2_sele_count:
            print('Warning: onlydonors2 selection empty!')    
            
    
    accres = result+"_aa"
    if onlyacceptors1_sele_count and onlyacceptors2_sele_count:
        aa_dist_out = cmd.distance(accres, 'onlyacceptors1_sele', 'onlyacceptors2_sele', cutoff, 0)

        if aa_dist_out < 0:
            print('\n\nCaught a pymol selection error in acceptor-acceptor selection of show_contacts')
            print('accres:', accres)
            print('onlyacceptors1', onlyacceptors1)
            print('onlyacceptors2', onlyacceptors2)
            return False
    
        cmd.set("dash_color","red",accres)
        cmd.set("dash_radius","0.125",accres)
    
    ########################################
    
    donres = result+"_dd"
    if onlydonors1_sele_count and onlydonors2_sele_count:
        dd_dist_out = cmd.distance(donres, 'onlydonors1_sele', 'onlydonors2_sele', cutoff, 0)
        
        #try to catch the error state 
        if dd_dist_out < 0:
            print('\n\nCaught a pymol selection error in dd selection of show_contacts')
            print('donres:', donres)
            print('onlydonors1', onlydonors1)
            print('onlydonors2', onlydonors2)
            print("cmd.distance('" + donres + "', '" + onlydonors1 + "', '" + onlydonors2 + "', " + str(cutoff) + ", 0)")  
            return False
        
        cmd.set("dash_color","red",donres)  
        cmd.set("dash_radius","0.125",donres)
    
    ##########################################################
    ##### find the buried unpaired atoms of the receptor #####
    ##########################################################
    
    #initialize the variable for when CALC_SASA is False
    unpaired_atoms = ''
    
    CALC_SASA = False
    
    if CALC_SASA:
        
        #For right now, I can't seem to figure out how to make a multi-state selection object of receptor atoms (1 frame) that coorespond 
        #to a selection in the various frames of the ligand.
        #So for now, we are gonna do it the likely inefficient way of doing it a frame at a time.
        
        num_frames = cmd.count_frames('ligs2')
        
        #iterate through the frames
        for frame in range(1, num_frames + 1):
            if SC_DEBUG > 1:
                print('\n\n\nframe:', frame)
            
            #this assumes that the receptor is the first object and that the ligand is the second, but it should work either way.... (famous last words)
            #Find all of the polar atoms on the protein
            rec_polar = selection  + " and (donor or acceptor) within 9 of (" + selection2 + " and state " + str(frame) + ')'
            lig_polar = "(" + selection2 + " and state " + str(frame) + ") and (donor or acceptor) within 6 of " + selection 
        
            cmd.select('rec_polar', rec_polar)
            cmd.select('lig_polar', lig_polar)
            
            polar_atoms = []
            apo_sasas = []
            complex_sasas = []
            unpaired_atoms = []
            bur_unpaired_coors = [] #coordinates of the buried unpaired atoms for creating pseudoatoms
            
            myspace = {'_my_area' : _my_area,
                       '_is_unpaired_atom' : _is_unpaired_atom,
                       'rec_polar' : rec_polar,
                       'lig_polar' : lig_polar,
                       'polar_atoms' : polar_atoms,
                       'apo_sasas' : apo_sasas,
                       'complex_sasas' : complex_sasas,
                       'unpaired_atoms' : unpaired_atoms,
                       'bur_unpaired_coors' : bur_unpaired_coors}
                
            #find the atoms on the receptor that are unpaired by the receptor
            cmd.iterate('rec_polar', "polar_atoms.append(_is_unpaired_atom(rec_polar, model, chain, resi, name))", space = myspace)
            
            if SC_DEBUG > 4:
                print('polar_atoms:', polar_atoms)
            
            #create a complex. Do it in two parts, first copy the ligand to state 1 and then select and combine them
            
            
            if SC_DEBUG > 3:
                print('create', 'foozle_ligand_hooha,', selection2 , ', source_state =', frame, ', target_state = 1, discrete = 1')
                print('sele temp_complex,', '(byres ' + selection + ' within 12 of foozle_ligand_hooha ) or foozle_ligand_hooha, target_state = 1')
            
            cmd.create('foozle_ligand_hooha', selection2 , source_state = frame, target_state = 1, discrete = 1)
            cmd.create('temp_complex', '(byres ' + selection + ' within 12 of foozle_ligand_hooha ) or foozle_ligand_hooha', target_state = 1)
            
            #cmd.create('temp_complex', '(byres ' + selection + ' within 12 of (' + selection2 + ' and state ' + str(frame) + ') ) or (' + selection2 + ' and state ' + str(frame) + ')', target_state = 1)
            
            #the _is_unpaired_atom function returns a list of selection strings of the unpaired atoms, select them from the complex
#            rec_polar_unpaired_str = '( ((' + ') or ('.join([ sele for sele in polar_atoms if sele ]) + ') ) and ' + selection + ' ) within 6 of (' + selection2 + ' and state ' + str(frame) + ')'
            rec_polar_unpaired_str = '( ((' + ') or ('.join([ sele for sele in polar_atoms if sele ]) + ') ) and ' + selection + ' ) within 6 of foozle_ligand_hooha'
            complex_polar_unpaired_str = '( ((' + ') or ('.join([ 'temp_complex ' + ' '.join( sele.split()[1:] ) for sele in polar_atoms if sele ]) + ') ) and temp_complex ) within 6 of foozle_ligand_hooha'
            
            
            if SC_DEBUG > 4:
                print('rec_polar_unpaired_str:', rec_polar_unpaired_str)
            
            
            rec_unpaired_atoms = cmd.select('rec_polar_unpaired', rec_polar_unpaired_str)
            complex_unpaired_atoms = cmd.select('complex_polar_unpaired', complex_polar_unpaired_str)
            
            if SC_DEBUG > 3:
                print('rec_unpaired_atoms:', rec_unpaired_atoms)
                print('complex_unpaired_atoms:', complex_unpaired_atoms)
                
            #cmd.show('dots', 'rec_polar_unpaired')
            
        
            #calculate the surface area of the unpaired atoms in the receptor alone and in the complex
            cmd.iterate('rec_polar_unpaired', "apo_sasas.append(_my_area(model, chain, resi, name))", space = myspace)
            cmd.iterate('complex_polar_unpaired', "complex_sasas.append(_my_area(model, chain, resi, name))", space = myspace)
            
            #Figure out which of them are unpaired (maybe do this first) #this time pass in the ligand polar atoms
            cmd.iterate('rec_polar_unpaired', 'unpaired_atoms.append(_is_unpaired_atom(lig_polar, model, chain, resi, name))', space = myspace)
            
            if SC_DEBUG > 3:
                print('len(complex_sasas):', len(complex_sasas))
                print('len(apo_sasas):', len(apo_sasas))
            assert len(complex_sasas) == len(apo_sasas), 'len(complex_sasas): ' + str(len(complex_sasas)) +  ' len(apo_sasas): ' + str(len(apo_sasas)) + '\n'
            assert len(complex_sasas) == len(unpaired_atoms)
            
            buried_unpaired = []
            
            for i in range(len(apo_sasas)):
                if SC_DEBUG > 4:
                    print(i)
                apo_sasa = apo_sasas[i]
                complex_sasa = complex_sasas[i]
                
                unpaired = unpaired_atoms[i]
                
                if unpaired: #if it is paired it will be None, else a string
                    d_sasa = apo_sasa - complex_sasa
                    #calculate the percent change
                    if apo_sasa > 0:
                        perc_sasa = d_sasa / apo_sasa
                    else:
                        perc_sasa = 0.0
                    
                    if SC_DEBUG > 3:
                        print('unpaired:', unpaired, 'd_sasa:', d_sasa, 'perc_sasa:', perc_sasa, 'apo_sasa:', apo_sasa, 'complex_sasa:', complex_sasa)
                    if perc_sasa > 0.25:
                        #it is buried and unpaired!
                        buried_unpaired.append(unpaired)
                        print('unpaired!', unpaired)
            
            if SC_DEBUG > 3:
                print('num buried unpaired:', len(buried_unpaired))
                print('(' + ') or ('.join(buried_unpaired) + ')')
            
            #check to see if there are any buried_unpaired atoms
            if buried_unpaired:
                buried_unpaired_atoms = cmd.select('buried_unpaired', '(' + ') or ('.join(buried_unpaired) + ')')
                
                if SC_DEBUG > 3:
                    print('buried_unpaired_atoms:', buried_unpaired_atoms)
                    
                    
                #now that we have the atoms that are buried and unpaired, pull out their xyz coordinates and create a pseudoatom and make it into a sphere
                
                #maybe find a faster way of doing this if I need to: http://www.pymolwiki.org/index.php/Get_Coordinates_I
                #the 1 is for iterating through the first frame (of temp_complex, which should only have one frame)
                cmd.iterate_state(1, 'buried_unpaired', 'bur_unpaired_coors.append([x,y,z])', space = myspace, atomic=0)
                
                unpaired_atoms = result + '_buried_unpaired'
                
                for xyz in bur_unpaired_coors:
                    cmd.pseudoatom(unpaired_atoms, pos = xyz, state = frame, name = 'O', vdw = 1.0, color = 'red')
                
                #show the psuedoatoms at spheres
                
                cmd.hide('everything', unpaired_atoms)
                cmd.show('spheres', unpaired_atoms)
                cmd.set('transparency', 0.65, unpaired_atoms)
            
            #cmd.show('dots', 'buried_unpaired')
            #cmd.show('spheres', 'buried_unpaired')
            #cmd.show('sticks', 'buried_unpaired')
            
            #starttime = time.time() ; show_contacts rec, lig, SC_DEBUG = 4 ; endtime = time.time() ; differ = endtime - starttime ; print 'time taken:', differ ;
            #starttime = time.time() ; show_contacts 1YCR, 1YCR_1, SC_DEBUG = 4 ; endtime = time.time() ; differ = endtime - starttime ; print 'time taken:', differ ;
            if SC_DEBUG < 5:
                cmd.delete('rec_polar')
                cmd.delete('lig_polar')
                cmd.delete('asdcanlsdcuntsaladsecn')
                cmd.delete('temp_complex')
                cmd.delete('rec_polar_unpaired')
                cmd.delete('complex_polar_unpaired')
                cmd.delete('buried_unpaired')
                cmd.delete('foozle_ligand_hooha')
                
        
    ## Group
    cmd.group(result,"%s %s %s %s %s %s" % (polres, allres, accres, donres, pol_ok_res, unpaired_atoms))
    
    ## Clean up the selection objects
    #if the show_contacts debug level is high enough, don't delete them.
    if SC_DEBUG < 5:
        cmd.delete('all_don_acc1_sele')
        cmd.delete('all_don_acc2_sele')
        cmd.delete('onlyacceptors1_sele')
        cmd.delete('onlyacceptors2_sele')
        cmd.delete('onlydonors1_sele')
        cmd.delete('onlydonors2_sele')
    
    #time how long it took
    endtime = time.time()
    elapsed_time = endtime - starttime
     
    if SC_DEBUG > 3:
        print('Done with show_contacts')
        print('Took', elapsed_time, 'seconds')
    
    return True
cmd.extend('show_contacts', show_contacts)

def parse_molecules(file):
    '''
    A generator function that reads in sdf files 
    one molecule at a time.
    Can also read in a compressed .sdf.gz file
    
    Arguments:
    file -- sdf(.gz) file name
    
    Returns:
    block -- list of strings of lines of the molecule
    '''
    if DEBUG > 2:
        print('file:', file) 
    if os.path.splitext(file)[1] == '.sdf': 
        with open(file) as lines:
            while True:
                block = list(itertools.takewhile(not_dollars, lines ))
                if not block:
                    break
                yield block + ['$$$$\n']
                
    elif os.path.splitext(file)[1] == '.gz': 
        with gzip.open(file,'rt') as lines:
            while True:
                block = list(itertools.takewhile(not_dollars, lines ))
                if not block:
                    break
                yield block + ['$$$$\n']
    else:
        raise ValueError('Error! parse_molecules needs a sdf file!')

def write_sdf(sdf_list, outfile = ''):
    '''
    Write out the list generated from parse_molecules to a tempfile.
    
    Arguments:
    sdf_list -- list returned from parse_molecules
    
    Returns:
    temp -- tempfile file object
    '''
    
    if outfile:
        temp = open(outfile, 'w')
    else:
        temp = tempfile.NamedTemporaryFile(mode='w+b', suffix='.sdf', delete=False)
    for sdf in sdf_list:
        temp.write(''.join(sdf))
    temp.close()
    return temp

def _sort_clus(sdffile, outfile, sort):
    '''
    Internally sort a cluster by a given critera and return the value of it's cluster level value
    
    Arguments:
    sdffile -- sdf file to sort
    outfile -- sorted sdf file
    sort -- critera to sort on. Can be size, Title, or an sdtag
    
    Returns:
    return [sdffile, outfile, sort_val]
    '''
    if DEBUG > 2:
        print('sorting', sdffile, 'by', sort)

    #if you are sorting based on size, just get the size and return
    if sort == 'size':
        sort_val = sdfsize(sdffile)
        shutil.copy2(sdffile, outfile) #copy the file to the output, maybe just set the outfile to be the sdffile
        return sdffile, outfile, sort_val
    
    else:
        tags = sdf_header(sdffile)
        
        #if it's an invalid tag, default to sorting by size
        if sort not in tags:
            if DEBUG > 1:
                print('Warning! Desired sd tag', sort, 'not found in', sdffile,'. Sorting by cluster size.')
            sort_val = sdfsize(sdffile)
            shutil.copy2(sdffile, outfile) #copy the file to the output, maybe just set the outfile to be the sdffile
            return sdffile, outfile, sort_val
        
        #read in all of the molecules
        mols = list(parse_molecules(sdffile))
        
        #list of lists of sort_val, mol
        tags_mols = []
        
        #get the value of the tag
        for mol in mols:
            #line that has the tag on it
            tag_line = 0
            for i, line in enumerate(mol):
                if re.search(f'^>\W+<{sort}>.*', line):
                    tag_line = i
                    break
            tag_value = mol[tag_line + 1].strip()
            
            #see if you can cast it to float
            try:
                tag_value = float(tag_value)
            except ValueError:
                pass
            
            #Add the sort value to the title -  replace AddToTitle from sdsorter
            mol[0] = mol[0].strip() + ';' + str(tag_value) + '\n'
            
            #if this is a tag known to require reverse sort, negate the value
            #TODO: add reverse sort option
            if sort == 'CNN_VS' or sort == 'CNNscore' or sort == 'CNNaffinity':
                tag_value *= -1
            tags_mols.append([tag_value, mol])
            
        tags_mols = sorted(tags_mols, key = lambda foo: foo[0])
                 
        write_sdf(list(zip(*tags_mols))[1], outfile)
        
        #get the tag of the top molecule as the cluster 
        sort_val = tags_mols[0][0]
            
    return [sdffile, outfile, sort_val]          
        
def _sort_clus_mult(args):
    '''
    Multiprocessing wrapper for _sort_clus
    
    Arguments:
    args -- list of arguments for _sort_clus
    
    Returns:
    _sort_clus returns
    '''
    return _sort_clus(*args)
     
def sort_clusters(outfiles, sort,  cpus = mp.cpu_count()):
    '''
    Sort clusters by selected critera. Copies files to temp files to rename
        
    Arguments:
    outfiles -- list of output file names
    sort -- string for what to sort clusters by. 'size' or valid SD tag
    cpus -- number of cpus to run on (Default: all available)
    
    Returns:
    sortedfiles -- renamed clusters
    '''
    if DEBUG > 2:
        print('sort_clusters: outfiles:', outfiles)
        
    cluster_affs = []
    
    #boolean for whether to use multiprocessing
    MULTI = True
    
    #make sure you aren't asking for more processors than you have
    if cpus > mp.cpu_count():
        if DEBUG > 1:
            print('Warning! Not enough CPUs to fulfill request of', cpus, 'reducing to', mp.cpu_count())
        cpus = mp.cpu_count()
        
    
    #list of commands to run
    cmds = [] 
    
    for outfile in outfiles:
        tmp = tempfile.NamedTemporaryFile(mode='w+b', suffix='.sdf', delete=False)
        if DEBUG > 3:
            print('args:',  [outfile, tmp.name, sort])
        args = [outfile, tmp.name, sort]
        cmds.append(args)
    
    if MULTI:
        #get a list of the similarity scores
        try:
            pool = mp.Pool(processes = cpus)
            result = pool.map_async(_sort_clus_mult, cmds) 
            cluster_affs = result.get()
            pool.close()
            pool.join()
            
        except KeyboardInterrupt:
            print('Caught KeyboardInterrupt. Terminating pool')
            pool.close()
            pool.terminate()
            pool.join()
    else:
        for i in cmds:
            _sort_clus(cmd[0], cmd[1], cmd[2])
    
    
#     cluster_affs.append([outfile, tmp.name, sort_val])
        
    if sort == 'size':
        reverse = True
    else:
        reverse = False
    
    if DEBUG > 3:
        print('cluster_affs:', cluster_affs)
    
    cluster_affs = sorted(cluster_affs, key = lambda foo: foo[2], reverse = reverse)
    
    
    if DEBUG > 1:
        print('Renaming files')
    
    #list of sorted files
    sortedfiles = []
    print(outfiles)
    #get the index of the last underscore of the cluster file name
    last_underscore = outfiles[0].rfind('_')
    #get the cluster name including the last underscore
    cluster_base = outfiles[0][:last_underscore + 1]
    
    for i, (orig, temp, aff) in enumerate(cluster_affs):
        #get the new name 
        newname = cluster_base + str(i) + '.sdf'
        
        #copy the temp file over
        os.rename(temp, newname)
        sortedfiles.append(newname)
        
        
    return sortedfiles
        
def extract_clusters(sdffile, outpref, extracts, sort = None, singletons = False, cpus = mp.cpu_count()):
    '''
    Read in the sdffile and split it into the clustered files.
    
    Arguments:
    sdffile -- input sdf file to extract from
    outpref -- output file name prefix - will append '_<num>.sdf' where num is the cluster number
    extracts -- list of lists of cluster members from 'hclust_fastcluster'
    sort -- string for what to sort clusters by. 'size' or valid SD tag
    singletons -- boolean, if True, combine all the singleton clusters into one
    
    Returns:
    outfiles -- output cluster files
    '''
    
#    outfiles = []
    
    if DEBUG > 1:
        print('Found', len(extracts), 'clusters. Extracting now...')
    
    if DEBUG > 4:
        print('extracts:', extracts)
        
    #Figure out which clusters are singletons
    singles = []
    non_singles = []
    
    if singletons:
        #find all of the singletons
        for clus in extracts:
            if len(clus) == 1:
                singles.append(clus)
            else:
                non_singles.append(clus)
    else:
        non_singles = extracts
    
    if singles:
        if DEBUG > 1:
            print('Found', len(singles), 'singletons. Grouping...')    
            print('Will produce', len(extracts) - len(singles) + 1, 'clusters')
    #make an array of cluster output files
#    outfiles = [ open(outpref + '_' + str(i) + '.sdf', 'w') for i in range(len(non_singles))]
    
    #make the list of output file names
    outfile_names = [ outpref + '_' + str(i) + '.sdf' for i in range(len(non_singles)) ]
    
    #make sure that they don't exist. If they do, delete them
    for outfile in outfile_names:
        if os.path.exists(outfile):
            os.remove(outfile)
    
    
    #if you are grouping singletons make a file for it and put it at the end of outfiles, access it through [-1]
    if singletons:
        #only do it if there are any
        if singles:
#            outfiles.append(open(outpref + '_singletons.sdf', 'w'))
            outfile_names.append(outpref + '_singletons.sdf')
    if DEBUG > 4:
        print('singles:', singles)
        print('non_singles:', non_singles)
    
    #now go through the sdf file and for each molecule, figure out which cluster it belongs to
    #and write it to that file. Should be O(N)
    for i,sdf in enumerate(parse_molecules(sdffile)):
        if DEBUG > 4:
            print('Compound:', i)
        sdf = ''.join(sdf)
        
        #if you are grouping singletons, first check to see if it is one
        if singletons:
            if singles: #if there are any
                is_single = [ clus for clus,clus_objs in enumerate(singles) if i in clus_objs ]
                if is_single: #if it is, write it out
                    #open the file, write out and close the file
                    fout = open(outfile_names[-1], 'a')
                    fout.write(sdf)
                    fout.close()
#                    outfiles[-1].write(sdf)
                    continue #move to the next iteration
            
        #otherwise check which cluster it's in
        clus = [ clus for clus,clus_objs in enumerate(non_singles) if i in clus_objs ]
        
        assert len(clus) == 1, 'Error! Something when wrong in extracting the clusters.\nI found compound ' + str(i) + ' in the following clusters: ' + ', '.join(clus) + '.\n' + \
                               'There are a couple of reasons that this could happen. Most likely, there is a saved result file (*.p) that was generated for a different set of compounds with the same file name. Delete any *.p files in /tmp/ or in your working directory: ' + os.getcwd() + ' \n' + \
                               'Your input file may also have errors. Try converting it with babel to another format (like mol2) to see if it gives any errors.'
        clus = clus[0]
        if DEBUG > 3:
            print('clus:', clus)
        
        #open the file, write out and close the file
        fout = open(outfile_names[clus], 'a')
        fout.write(sdf)
        fout.close()
#        outfiles[clus].write(sdf)
        
    #now that everything has been written to, close all of the files
#    for fout in outfiles:
#        fout.close()
        
    
    if sort:
        if DEBUG > 1:
            print('Sorting clusters by', sort)
            
        if singletons:
            if DEBUG > 2:
                print('Sorting excluding singletons')
            
            #if you are grouping the singletons, only sort the other ones
#            tmp = [ fname.name for fname in outfiles if not fname.name.endswith('_singletons.sdf') ]
            tmp = [ fname for fname in outfile_names if not fname.endswith('_singletons.sdf') ]
            if DEBUG > 3:
                print('tmp:',tmp)
                
            #now grab the singleton cluster and put it on the end
#            singleton_file_names = [ fname.name for fname in outfiles if fname.name.endswith('_singletons.sdf') ]
            singleton_file_names = [ fname for fname in outfile_names if fname.endswith('_singletons.sdf') ]

            if DEBUG > 2:
                print('singleton_file_names:', singleton_file_names)
            
            #if all the clusters are singletons. Don't sort (it will crash)
            if len(tmp) > 0:
                #sort the non-singletons
#                outfiles = sort_clusters(tmp, sort = sort, cpus = cpus)
                outfile_names = sort_clusters(tmp, sort = sort, cpus = cpus)
            else:
#                outfiles = []
                outfile_names = []
            #put the singlton on the end
            outfile_names.extend(singleton_file_names)
            
        
        else:
            outfile_names = sort_clusters([ f for f in outfile_names], sort = sort, cpus = cpus)
#    else:
#        #if you aren't sorting, just pull out the names
#        outfiles = [ f.name for f in outfiles]
#    
    if DEBUG > 2:
        print('outfile_names:', ' '.join(outfile_names))
    
    #There is a sutlety here, on initialization, the `outfiles` object held file objects, but after
    #sorting, it is now holding just the file names. 
    #sorry, http://blog.codinghorror.com/curlys-law-do-one-thing/
    #update 3-19-15, not anymore!
    return outfile_names

def hclust_fastcluster(sim_list, outfile, cutoff):
    '''
    Use fastcluster to compute the hieracrhial clustering. 
    It should scale a lot better (O(N^2) vs. O(N^3)).
    http://danifold.net/fastcluster.html?section=1 
    
    Arguments:
    sim_list -- distance matrix
    outfile -- pickle file to save clustering results to
    cutoff -- height to cutoff 
    
    
    Returns:
    extracts -- list of lists assigning cluster membership
    '''
    
    if DEBUG > 1:
        print('Computing clustering with fastcluster')
    
    #if there is only one observation, you can't cluster
    if len(sim_list) == 1:
        extracts = [[0]]
        return extracts
    
    #save the hierchical clustering so you don't have to recompute it, just recut
    if not os.path.exists(outfile) or REDO:
        Z = fastcluster.linkage(sim_list) #THE DIFFERENCE
        with open(outfile,'wb') as out: 
            pickle.dump(Z, out,protocol=0)
        if DEBUG > 2:
            print('Clustering results saved to', outfile)
    else:
        Z = pickle.load(open(outfile, 'rb'))    
    if DEBUG > 4:
        print('Z:', Z)
    
    if SHOW_PLOT:
        plt.figure()
        scipy.cluster.hierarchy.dendrogram(Z, color_threshold = cutoff)
        plt.ylabel("Distance [AU]", fontsize = 16)
        plt.xlabel("Compounds", fontsize = 16)
        plt.gca().tick_params(axis = 'x', which = 'both', top = 'off', bottom = 'off')
        plt.gca().tick_params(axis = 'y', which = 'both', right = 'off')
        plt.xticks([])
        plt.gca().spines['right'].set_visible(False)
        plt.gca().spines['top'].set_visible(False)
        xmin,xmax = plt.xlim()
        plt.plot([xmin,xmax], [cutoff, cutoff], '--k')
        plt.tight_layout()
        figname = os.path.splitext(os.path.basename(outfile))[0] + '_dendrogram.png'
        plt.savefig(figname)
    
    
    if DEBUG > 2:
        maxdists = scipy.cluster.hierarchy.maxdists(Z)
        maxdist = max(maxdists)
        print('Maximum tree hieght:', maxdist)
    
    #cut at the desired heights, still with scipy, as fast cluster doesn't seem to have one
    print("Cutoff",cutoff)
    cuts = scipy.cluster.hierarchy.fcluster(Z, cutoff, criterion='distance')
    
    #scipy counts from 1, but we need to be counting from 0, so subtract 1 from everything
    cuts = cuts - 1
    
    if DEBUG > 3:
        print('cuts:', cuts)
        
    #pull out the cluster members
    extracts = []
    
    for i in range(np.max(cuts) + 1):
        idxs = list(np.where(cuts == i)[0])
        extracts.append(idxs)
    
    if DEBUG > 5:
        print('extracts:', extracts)
    return extracts

def rename_mols(infile):
    '''
    Renames the input molecules 
    
    Arguments:
    infile -- input sdf file
    
    Returns:
    temp2 -- output sdf file (tempfile object)
    '''
    if DEBUG > 1:
        print('Starting pre-processing')
    if DEBUG > 3:
        print('rename_mols:', infile)
    #strip the current title
    temp1 = tempfile.NamedTemporaryFile(mode='w+b', suffix='.sdf',  delete=False)
    cmmd = [BABEL, '--title', " ",'-O'+temp1.name]
    if DEBUG > 2:
        print('cmmd:', cmmd)
        print(' '.join(cmmd))
    p = sp.Popen(cmmd, stdout = sp.PIPE, stderr = sp.PIPE)
    out,err = p.communicate()
    
    if err != None:
        print(err)
    
    #add the input index to the title - indexed from 1
    temp2 = tempfile.NamedTemporaryFile(mode='w+b', suffix='.sdf',  delete=False)
    cmmd = [BABEL, temp1.name,'--AddInIndex', '-O'+temp2.name]
    if DEBUG > 2:
        print(' '.join(cmmd))
    p = sp.Popen(cmmd, stdout = sp.PIPE, stderr = sp.PIPE)
    out,err = p.communicate()
    
    if err != None:
        print(err)

    #delete the first temp file
    temp1.close()
    os.remove(temp1.name)
    
    temp2.close()
    
    if DEBUG > 1:
        print('Pre-processing done')
    if DEBUG > 3:
        print('rename_mols:', temp2.name)
    return temp2


fingerprints = []
def compute_row(i):
    return 1.0-np.array([fingerprints[i]|fp for fp in fingerprints])    
            
def compute_sim_matrix(infile, outfile = '', cpus = 4):
    '''
    Compute the similarity matrix of the ligands
    
    Arguments:
    infile -- ligand file name
    outfile -- picklefile name
    cpus -- how many cpus to run on
    
    Returns:
    all_sims -- list of lists of similarity scores
    '''
    
    global RUNNING
    global REDO
    global fingerprints #so we can reference in subprocesses without having to pickle
    
    #do some input validation
    if not os.path.exists(infile):
        if DEBUG > 0:
            print('Error! Input file not found!', infile)
        return []
    
    if sdfsize(infile) == 0:
        if DEBUG > 0:
            print('Error! No compounds found in the input file!', infile)
        return []
                
    
    if not outfile:
        outfile = os.path.splitext(os.path.basename(infile))[0] + '_pickle.p'
        
    if not os.path.exists(outfile) or REDO:
        all_sims = []
        
        if DEBUG > 2:
            print('calculate fingerprints')        
            
        fingerprints = [mol.calcfp() for mol in pybel.readfile('sdf',infile)]
                    
        if DEBUG > 1:
            print('Computing distance matrix')
        
        N = len(fingerprints)
        distances = np.ones((N,N))
    
        pool = mp.Pool(processes = cpus)
        distances = np.array(pool.map(compute_row, range(N))) 
        
        #save to a pickle file
        if DEBUG > 1:
            print('Saving distances to file', outfile)
        with open(outfile, 'wb') as out:
            pickle.dump(distances, out,protocol=0)        

    else:
        if DEBUG > 0:
            print('Pickle file already found:', outfile)

        distances = pickle.load(open(outfile, 'rb'))
        if DEBUG > 0:
            print('Similarity matrix loaded')
    RUNNING = False
    if DEBUG > 4:
        print('RUNNING = False')
    
    if DEBUG > 0:
        print('Done')
    return distances

def cluster_and_extract(all_sims, infile, cutoff = 2.0, singletons = True, sort = 'size', dry_run = False):
    '''
    Cluster and extract the compounds.
    
    Arguments:
    all_sims -- similarity matrix
    infile -- input file (sdf)
    cutoff -- how high to cut the hierarchical clustering
    singletons -- boolean for whether to group singleton clusters
    sort -- string for what to sort clusters by. 'size' or valid SD tag
    dry_run -- boolean, if true don't actually extract the clusters, just count how many there are
    
    Return:
    num_clusters -- if dry_run is specified, it returns the number of clusters
    outfiles -- output files
    '''
    
    #do the clustering
    if DEBUG > 1:
        print('Computing hierarchical clustering')

    #create the pickle file name        
    picklefile = tempfile.gettempdir() + os.sep + os.path.splitext(os.path.basename(infile))[0] + '_linkage.p'

    extracts = hclust_fastcluster(all_sims, picklefile, cutoff)
    
    #if this is just a dry run, just return the count of how many there are
    if dry_run:
        #count the number of singletons
        if singletons:
            num_singletons = max(0, len([ 1 for clus in extracts if len(clus) == 1 ]) - 1)
        else:
            num_singletons = 0
        print('extracts:',extracts)
        print('num_singletons:', num_singletons)
        print('len(extracts):', len(extracts))
        num_clusters = len(extracts) - num_singletons
        print('Will produce', num_clusters, 'clusters')
        return num_clusters
    else:
        if DEBUG > 1:
            print('Extracting clusters')
            
        outpref = tempfile.gettempdir() + os.sep + os.path.splitext(os.path.basename(infile))[0] + '_cluster'
        #extract the clusters using the original input file
        outfiles = []
        outfiles = extract_clusters(infile, outpref, extracts, sort = sort, singletons = singletons)
        
        return outfiles

def sdf_header(sdffile):
    '''
    Get the sd tags from the sdf file 
    Because you can't call this from within the plugin class
    
    Arguments:
    sdffile -- sdf file 
    
    Returns:
    header -- list of header strings
    
    TODO: This should probably be cached in a global variable because we should be able to assume that the sd tags will be the same in all molecules in the input file. 
    '''
    if not os.path.exists(sdffile):
        raise OSError('Error! File not found: ' + str(sdffile) + '\n')
    
    sdmol = next(parse_molecules(sdffile))
    
    #parse out the tag manually
    raw_tags = [ line for line in sdmol if line.startswith('>') ]
    if DEBUG > 3:
        print('sdf_header: sdffile:', sdffile)
        print('sdf_header: raw_tags:', raw_tags)
    headers = [ re.search('>\s*<(.*)>.*', line).group(1) for line in raw_tags ]
    
    return headers


def parse_args():
    '''
    Parse command line args
    
    Returns:
    sys_args -- dict with the following keys:
            ligands -- input file
            pickle -- pickle file to save to
            cutoff -- cutoff height
            singletons -- boolean for whether to group singletons
            sort -- boolean for whether to sort clusters by energy
    '''
    global REDO
    global DEBUG
    
    import argparse
    
    parser = argparse.ArgumentParser(description = 'Cluster input compounds based on substructure')
    parser.add_argument('ligands',  nargs = 1, help = 'ligand file (.sdf)')
    parser.add_argument('-p', dest = 'pickle',  required = False, default = '', help = 'pickle file for saving data and naming clusters')
    parser.add_argument('-c', dest = 'cutoff',  nargs = 1, required = False, type = float, default = [2.0], help = 'Height to cut off hierarchical cluster (positive float, default = 2)')
    parser.add_argument('--no_singletons', dest = 'singletons',  action = 'store_const', const = False, default = True, help = 'If specified, do not group clusters with one member. Default: True')
    parser.add_argument('--sort', dest = 'sort',  nargs = 1, required = False, default = ['size'], help = 'Critera to sort clusters by. Default is sort by size, also accepts any valid SD tag')
    parser.add_argument('--redo', dest = 'redo', action = 'store_const', const = True, default = False, help = 'If specified, ignore any saved results (pickle files) and recompute')
    parser.add_argument('--cpus', dest = 'cpus', nargs = 1, required = False, type = int, default = [ mp.cpu_count() ], help = 'Number of CPUs to use. Default: detect number available' )
    parser.add_argument('-v', dest = 'DEBUG', required = False, type = int, default = 2, help = 'Debugging level to use. Default: 2 (higher is more, effective range 0-6)')
    #parser.add_argument('--cluster_funct', dest = 'cluster_funct',  required = False, default = 'fastcluster', help = 'Choose clustering method: fastcluster, scipy or self')
    parser.add_argument('--version', dest = 'version', action = 'store_const', const = True, default = False, help = 'Print version information.')
    parser.add_argument('--dry-run', dest = 'dryrun',  action = 'store_const', const = True, default = False, help = 'If specified, do not actually extract the clusters, just count them. Default: False')
    args = parser.parse_args()
    
    
    if args.version:
        print('cluster_mols.py', VERSION)
        sys.exit()
    
    ligands = args.ligands[0]
    pickle = args.pickle
        
    cutoff = args.cutoff[0]
    assert cutoff >= 0, "Cutoff must be greater then 0"
    singletons = args.singletons
    sort = args.sort[0]
    REDO = args.redo
    cpus = args.cpus[0]
    DEBUG = args.DEBUG
    dry_run = args.dryrun
    
    #throw them into a dict
    sys_args = {}
    sys_args['ligands'] = ligands
    sys_args['pickle'] = pickle
    sys_args['cutoff'] = cutoff
    sys_args['singletons'] = singletons
    sys_args['sort'] = sort
    sys_args['cpus'] = cpus
    sys_args['dry_run'] = dry_run
    
    return sys_args

def main():
    #parse the command line arguments
    sys_args = parse_args()
        
    infile = sys_args['ligands']
    outfile = sys_args['pickle']
    cutoff = sys_args['cutoff']
    singletons = sys_args['singletons']
    sort = sys_args['sort']
    cpus = sys_args['cpus']
    dry_run = sys_args['dry_run']
    #temp
#     cluster_funct = sys_args['cluster_funct']
    
    assert os.path.exists(infile), 'Error! File not found: ' + str(infile) + '\n'
    
    #make the output file name
    if not outfile:
        outfile = '/tmp/' + os.path.splitext(os.path.basename(infile))[0] + '_pickle.p'
    

    all_sims = compute_sim_matrix(infile, outfile, cpus = cpus)    

    if type(all_sims) != type(None):
    
        outfiles = cluster_and_extract(all_sims, infile, cutoff = cutoff, singletons = singletons, sort = sort, dry_run = dry_run)
    
        if dry_run:
            print('Dry Run: Will produce', outfiles, 'clusters')
    
        else:
            if len(outfiles) == 1:
                print('Warning! Only one cluster was found! Lower the cutoff (-c) to get more.')
            


    
    
    
#################################################################################
#################################################################################
########################### Start of pymol plugin code ##########################
#################################################################################
#################################################################################

default_settings = {
                    'cutoff' : 1.5,
                    'cpus' : mp.cpu_count(),
                    'singletons' : True,
                    'sort' : 'minimizedAffinity',
                    'contacts' : True,
                    'contacts_selection' : "polymer",
                    'redo' : REDO
                    }
    




#some help texts

welcome_text = ''' Welcome to cluster_mols! 

This tool is designed to aid in virtual screening. 
    Say you are conducting a virtual screen and have done an inital filtering, maybe by pharmacophore searching followed by energy minimization and filtering. Now you need to look through the compounds and pick some of them to buy and test. Typically, you would load them up and look through them one at a time and somehow select the ones you want.

    cluster_mols aids the user through many sof these tasks by clustering the compounds by scaffold and automaticially loads them into pymol and shows the polar contacts 
with a specified protein.  This script also provided keyboard controls for navigating these clusters (Ctrl+WASD) and selecting compounds for later analysis. If the compounds are named starting with a ZINC or Molport ID, it can also check online to see if it is available. 
See the About tab for more info.
'''

multiprocessing_help_text = '''Specify how many processors you want to run on.'''

compute_similarity_help_text = '''Start the similarity computation in the background. Depending on how many compounds you have, this could take a while. The results will be saved after it runs, so this will only need to be run once. 
Normal runtime: Scales O(N^2) with number of compounds: on the order of minutes'''

sort_clusters_help_text = '''Specify how to sort the clusters. Good places to start are sorting by 'size', which makes the cluster with the most members into cluster 0. Also try sorting by 'minimizedAffinity' if you are working with compounds docked/minimized by smina. This sorts the clusters by their best affinity. So the lowest energy pose of all compounds will be the first molecule of cluster 0. CNN_VS, CNNscore, and CNNaffinity are reverse sorted.'''

cutoff_help_text = '''The compounds are clustered using hierarchical clustering. Choose how high to cut the tree for cluster creation. Higher numbers lead to fewer clusters with less similar compounds and lower numbers lead to more clusters of highly similar compounds.  Default: 1.5'''

group_singletons_help_text = '''Choose whether to group clusters with only one member into a common cluster. Useful when cutting the tree at low values.'''

group_contacts_help_text = '''Compute and visualize the polar contacts between ligands and specified selection.'''

make_clusters_help_text = '''(Re)run the clustering and break compounds into clusters, load them into pymol and group them. This deletes any existing group that the plugin created previously. Scales with the number of compounds and the number of clusters (controlled by the cutoff) Normal runtime: ~1 minute.'''

about_text = '''Cluster_mols speeds up the virtual screening process by automating a number of tasks for the user. By clustering the compounds by scaffold, the user is able to quickly decide if he or she likes the scaffold. If so, they can easily flip through the different compounds in the scaffold and select those that have the best substituents. If not, they can quickly move on and not waste any time later looking at other compounds from that scaffold. 

The basic work flow of cluster_mols.py can be broken up into two main parts, which are controlled through two tabs in the interface:

Compute Similarities:
    - Computes a pairwise similarity matrix from the input compounds
    - Performing hierarchical clustering on the results matrix
Cluster Compounds:
    - Cutting the tree at a user-specified height and creating and sorting clusters 

There are also keyboard controls that allow for navigating the clusters and selecting compounds. You can navigate the clusters using the WASD keys (you have to press the Ctrl or Alt key). If you find a compound that you like, you can hit one button (see below) and it will be copied into a new pymol object where you can examine it later or save it to a file.

If your compounds have names that start with ZINC or Molport IDs, the program can check online and see if they are available. 

Keyboard Controls:
    Cluster Navigation:
        'CTRL-W','ALT-W' -- Move up to the next cluster
        'CTRL-S','ALT-S' -- Move down to the next cluster
        'CTRL-A','ALT-A' -- Decrease frame
        'CTRL-D','ALT-D' -- Increase frame
    Selected Compound List Manipulation:
        'F1' -- Print currently selected molecule
        'F2' -- Remove most recently added compound
        'F3' -- Add currently visible compound to list
        'F4' -- Print List
    Compound Purchasability Information:
        'CTRL-F','ATL-F' -- Check for available vendors

cluster_mols is developed by Matt Baumgartner with Dr. David Koes and Dr. Carlos Camacho.
Feel free to contact me (mpb21@pitt.edu) if you find a bug or have feature suggestions.
'''

class Cluster_Mols:
    ''' Main Pymol Plugin Class '''
    def __init__(self, app):
        global RUNNING
        global KILL
        RUNNING = False #try to reinitialize it
        
        parent = app.root
        self.parent = parent
        
        self.app = app
        
        if DEBUG > 3:
            print('getting ligand_file')
        self.ligand_file = ''
        
                #variable to dispalay the loaded ligand file
        self.display_ligand_file = tkinter.StringVar()
        self.display_ligand_file.set(self.ligand_file)
        if not self.ligand_file:
            self.display_ligand_file.set('No ligand file selected.')
        
        
        
        ############################################################################################
        ### Open a window with options to select a ligand from a file or from the loaded objects ###
        ############################################################################################

        self.ligand_select_dialog = Pmw.Dialog(parent, 
                         buttons = ('Ok','Cancel'), 
                         title = 'Cluster_mols Plugin -- Cluster Compounds - ' + VERSION,
                         command = self.ligand_button_pressed )
    
        self.ligand_select_dialog.withdraw()
    
        #load ligand from file button
        self.ligand_select_file_button_box = Pmw.ButtonBox(self.ligand_select_dialog.interior(), padx=0, pady=1, orient='vertical')
#        self.ligand_select_file_button_box.pack(side=TOP)
        self.ligand_select_file_button_box.add('Select Ligand from File...', command = self.reload_ligands_dialog_pressed,
                                        background = HIGHLIGHT_COLOR,
                                        activebackground = HIGHLIGHT_COLOR_ACTIVE)
        
        
        self.ligand_select_file_button_box.grid(column=0, row=0)
        
        #allow the user to select from objects already loaded in pymol
        self.ligand_select_object_combo_box = Pmw.ComboBox(self.ligand_select_dialog.interior(),
                                                               scrolledlist_items=[],
                                                               labelpos='w',
                                                               label_text='Select loaded object:',
                                                               listbox_height = 2,
                                                               selectioncommand = self.select_loaded_object_command,
                                                               dropdown=True)
        #add the current objects to the list
        #self.populate_ligand_select_list()
        #We need to defer populating this list until the selection box in the main window is created a little further down. 
        #So we won't call it here, but when we call it later after the other list is created (but not shown) this one will be populated too.
            
        
        self.ligand_select_object_combo_box.grid(column=1, row=0)
        
        #display the selected file name
        self.ligand_select_loaded_file_frame = tkinter.Frame(self.ligand_select_dialog.interior())
        self.ligand_select_loaded_file_label = tkinter.Label(self.ligand_select_loaded_file_frame, text = '')
        self.ligand_select_loaded_file_location = tkinter.Entry(self.ligand_select_loaded_file_frame,
                                                  textvariable = self.display_ligand_file,
                                                  bg = 'black',
                                                  fg = 'white',
                                                  width = 60)
        
        self.ligand_select_loaded_file_label.pack(side = BOTTOM)
        self.ligand_select_loaded_file_location.pack(side = BOTTOM, padx = 5)
        
        self.ligand_select_loaded_file_frame.grid(column=0, row=1, columnspan = 2, pady = 4)
        

        
        
        ############################################################################################
        ############################## The main window ############################################# 
        
        #this window is just constructed here, it is shown after the first window is closed. The call is in ligand_button_pressed
        

        #the similarity score matrix
        self.all_sims = []
        
        #pickle file used for saving and loading the similarities
        self.picklefile = self.get_pickle_file_name()
        
        #boolean checkbox for sorting and singletons
        self.singletons = tkinter.BooleanVar()
        self.singletons.set(default_settings['singletons'])
        
        self.contacts = tkinter.BooleanVar()
        self.contacts.set(default_settings['contacts'])
        
        self.contacts_selection = tkinter.StringVar()
        self.contacts_selection.set(default_settings['contacts_selection'])
        
        #count of the number of clusters
        self.cluster_count = tkinter.StringVar()
        self.cluster_count.set('')
        
        #boolean check box for redo button
        self.redo = tkinter.BooleanVar()
        self.redo.set(default_settings['singletons'])
        
        
#        if not self.ligand_file:
#            return
        
        self.cutoff = tkinter.DoubleVar()
        self.cutoff.set(default_settings['cutoff'])
        
        self.cpus = tkinter.IntVar()
        self.cpus.set(default_settings['cpus'])
        
        self.display_options = {
                'redo' : default_settings['redo'],
                'cutoff' : default_settings['cutoff'],
                'cpus' : default_settings['cpus']
                }
        
        
        self.dialog = Pmw.Dialog(parent, 
                                 buttons = ('Ok',), 
                                 title = 'Cluster_mols Plugin -- Cluster Compounds - ' + VERSION,
                                 command = self.button_pressed )
    
        self.dialog.withdraw()
    
        Pmw.setbusycursorattributes(self.dialog.component('hull'))
    
        self.dialog.geometry('650x800')
        self.dialog.bind('<Return>', self.button_pressed)
        
        title_label = tkinter.Label(self.dialog.interior(),
                                    text = 'PyMOL Compound Clustering\nMatthew Baumgartner -- mpb21@pitt.edu\nDavid Ryan Koes, and Carlos Camacho',
                                    background = 'white',
                                    foreground = 'black',
                                    )
        title_label.pack(expand = 0, fill = 'both', padx = 4, pady = 4)
    
        #main notebook
        self.notebook = Pmw.NoteBook(self.dialog.interior())
        self.notebook.pack(fill = 'both', expand = 1, padx = 3, pady = 3)
        
        
        #####################################################
        ######### add a page for computing similarity #######
        ##################################################### 
        #for code folding
        if True:
            self.option_page = self.notebook.add('Compute Similarities')
            
            
            
            self.option_page_welcome_group = Pmw.Group(self.option_page, tag_text='Welcome')
            self.option_page_welcome_group.pack(fill = 'both', expand = 0, padx = 10, pady = 5)
    
            self.welcome_text_field = tkinter.Label(self.option_page_welcome_group.interior(),
                                             text = welcome_text,
                                             background = 'white',
                                             foreground = 'black',
                                             justify = LEFT,
                                             )
            self.welcome_text_field.pack(expand = 0, fill = 'both', padx = 4, pady = 4)
            
            #Add a message to follow the highlighed buttons
            if HIGHLIGHT_BUTTONS:
                self.welcome_text_field = tkinter.Label(self.option_page_welcome_group.interior(),
                                                 text = 'Follow the GREEN buttons!',
                                                 background = 'white',
                                                 foreground = 'green',
                                                 justify = LEFT,
                                                 font = ('TkDefaultFont', 16)
                                                 )
                self.welcome_text_field.pack(expand = 0, fill = 'both', padx = 4, pady = 4)
    
            
            #section for selecting a new ligand
            self.option_page_load_ligand_group = Pmw.Group(self.option_page, tag_text = 'Input Ligand')
            self.option_page_load_ligand_group.pack(fill = 'both', expand = 1, padx = 10, pady = 0)
            
            #section for selection number of CPUS
            self.option_page_multi_options = Pmw.Group(self.option_page, tag_text='Multiprocessing Options')
            self.option_page_multi_options.pack(fill = 'both', expand = 1, padx = 10, pady = 0)
            
    
            #section for running the similarity search
            self.option_page_compute_sims = Pmw.Group(self.option_page, tag_text='Compute Similarities - Results are saved to a file after running.')
            self.option_page_compute_sims.pack(fill = 'both', expand = 0, padx=10, pady=5)
            
            
            
            #show a button for reloading the ligand file and displaying the current file
            # display option buttons
            self.load_ligand_button_box = Pmw.ButtonBox(self.option_page_load_ligand_group.interior(), padx=0, pady=1,orient='vertical')
            self.load_ligand_button_box.add('Select Ligand from File...', command = self.reload_ligands_dialog_pressed,
                                            background = HIGHLIGHT_COLOR,
                                            activebackground = HIGHLIGHT_COLOR_ACTIVE)
#            self.load_ligand_button_box.pack(side=LEFT)
            self.load_ligand_button_box.grid(column=0, row=0)
            
            #allow the user to select from objects already loaded in pymol
#            loaded_objects = [ name for name in cmd.get_names() if '_cluster_' not in name ] 
            
            self.select_ligand_combo_box = Pmw.ComboBox(self.option_page_load_ligand_group.interior(),
                                                                   scrolledlist_items=[],
                                                                   labelpos='w',
                                                                   label_text='Select loaded object:',
                                                                   listbox_height = 2,
                                                                   selectioncommand = self.select_loaded_object_command,
                                                                   dropdown=True)
            #add the current objects to the list
            #this also silently populates the list in the ligand select window as well. It's a little more confusing, 
            #but it should be more consistent because I don't have to remember to call two functions every time
            self.populate_ligand_select_list()
            
            #self.select_ligand_combo_box.pack(side=RIGHT, padx=0)
            self.select_ligand_combo_box.grid(column=1, row=0)
            
            self.loaded_file_frame = tkinter.Frame(self.option_page_load_ligand_group.interior())
            self.loaded_file_label = tkinter.Label(self.loaded_file_frame, text = '')
            self.loaded_file_location = tkinter.Entry(self.loaded_file_frame,
                                                      textvariable = self.display_ligand_file,
                                                      bg = 'black',
                                                      fg = 'white',
                                                      width = 70)
            
            self.loaded_file_label.pack(side = BOTTOM)
            self.loaded_file_location.pack(side = BOTTOM, padx = 5, anchor = 's')
#            self.loaded_file_frame.pack(side = BOTTOM, padx = 4, pady = 1)
            
            self.loaded_file_frame.grid(column=0, row=1, columnspan=2)
            
            #Add a input for the cpus
            self.cpu_frame = tkinter.Frame(self.option_page_multi_options.interior())
            self.cpu_label = tkinter.Label(self.cpu_frame, text = 'CPUs')
            self.cpu_location = tkinter.Entry(self.cpu_frame, 
                                                       textvariable = self.cpus, 
                                                       bg = 'black', 
                                                       fg = 'white', 
                                                       width = 10)
            
            
            self.cpu_label.pack(side = 'left')
            self.cpu_location.pack(side = 'left', padx = 5)
            self.cpu_frame.pack(side = 'left', padx = 4, pady = 1)
            
            
            #add help text for the number of cpus to use
            self.cpu_frame_help_text = tkinter.Label(self.option_page_multi_options.interior(),
                                                            pady = 10,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = multiprocessing_help_text
                                                            )
            self.cpu_frame_help_text.pack(side='right', pady=1, padx=4)
            
            
            
            # Compute similarities subbox
            self.redo_checkbox = Pmw.RadioSelect(self.option_page_compute_sims.interior(),
                                            selectmode='single',
                                            buttontype='checkbutton',
                                            labelpos='w',
                                            label_text='Ignore saved results?',
                                            orient='vertical',
                                            frame_relief='ridge',
                                            command=self.redo_checkbox_changed)
            self.redo_checkbox.pack(side = TOP, padx = 10, anchor = 'w')
            
            self.redo_checkbox.add('redo', text = '')
            
            #initialize check
            if not self.redo.get():
                self.redo_checkbox.invoke('redo')
            
            
            self.compute_sim_button_box = Pmw.ButtonBox(self.option_page_compute_sims.interior(), padx=4, pady=1, orient='vertical')
            self.compute_sim_button_box.pack(side=LEFT)
            self.compute_sim_button_box.add('\n\n   Compute Similarity    \n\n',
                                            command = self.comp_sims_button_pressed,
                                            background = '#d9d9d9', #default gray '#d9d9d9'
                                            foreground = 'black',
                                            highlightcolor = '#d9d9d9')
            
            #add help text for computing similiarty
            self.compute_sim_button_help_text = tkinter.Label(self.option_page_compute_sims.interior(),
                                                            pady = 10,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = compute_similarity_help_text)
            
            self.compute_sim_button_help_text.pack(side='right', pady=1, padx=2)
            
            
            
        
        #####################################################
        ############### add a page for clustering ###########
        ##################################################### 
        #for code folding
        if True:
            #build option page
            self.cluster_page = self.notebook.add('Cluster Compounds')
            
            self.cluster_page_sortby = Pmw.Group(self.cluster_page, tag_text='Sort Clusters. None = As is. Try size or minimizedAffinity')
            self.cluster_page_sortby.pack(fill = 'both', expand = 1, padx = 10, pady = 1)
            
            #cutoff slider
            self.cluster_page_cutoff = Pmw.Group(self.cluster_page, tag_text = 'Height to cut hierarchical cluster (float > 0)')
            self.cluster_page_cutoff.pack(fill = 'both', expand = 1, padx = 10, pady = 1)
            
            #singletons
            self.cluster_page_singletons = Pmw.Group(self.cluster_page, tag_text = 'Group Singleton Clusters into one Cluster?')
            self.cluster_page_singletons.pack(fill = 'both', expand = 1, padx = 10, pady = 1)
    
            #contacts
            self.cluster_page_contacts = Pmw.Group(self.cluster_page, tag_text = 'Compute polar contacts')
            self.cluster_page_contacts.pack(fill = 'both', expand = 1, padx = 10, pady = 1)
                    
            self.cluster_page_cluster = Pmw.Group(self.cluster_page, tag_text = '(Re)run clustering')
            self.cluster_page_cluster.pack(fill = 'both', expand = 1, padx = 10, pady = 1)
            
            ########## Add a dropdown list for sorting ###############
            self.sort_list = Pmw.ComboBox(self.cluster_page_sortby.interior(),
                                                               scrolledlist_items=[],
                                                               labelpos='nw',
                                                               label_text='Sort by:',
                                                               listbox_height = 2,
                                                               selectioncommand = self.selectionCommand,
                                                               dropdown=True)
            self.sort_list.pack(side=LEFT, padx=0, anchor='n')
            
            #populate the list of headers
            self.populate_sort_list()
            
            #add help text for sorting
            self.sort_list_help_text = tkinter.Label(self.cluster_page_sortby.interior(),
                                                            pady = 5,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = sort_clusters_help_text)
            self.sort_list_help_text.pack(side='right', pady=1, padx=4)
            
            
            ########## Add a input for cutting the tree ###############
            self.cut_frame = tkinter.Frame(self.cluster_page_cutoff.interior())
            self.cut_label = tkinter.Label(self.cut_frame, text = 'Cutoff')
            self.cut_frame.pack(side = LEFT, padx = 4, pady = 1)
            
            self.cut_location = tkinter.Entry(self.cut_frame, 
                                                       textvariable = self.cutoff, 
                                                       bg = 'black', 
                                                       fg = 'white', 
                                                       width = 10)
            self.cut_scrollbar = tkinter.Scrollbar(self.cut_frame, orient='horizontal', command = self.cutoff_changed)
            
#            self.cut_label.pack(side = LEFT, anchor='w')
#            self.cut_location.pack(side = LEFT, padx = 5, anchor='nw')
#            self.cut_scrollbar.pack(side = LEFT, anchor='w')
#            self.cut_frame.pack(side = LEFT, padx = 4, pady = 1)
            self.cut_frame.grid(column = 0, padx = 5, pady = 10)
            self.cut_label.grid(column = 0, row = 0, padx = 5, pady = 10)
            self.cut_location.grid(column = 1, row = 0, padx = 5, pady = 10)
            self.cut_scrollbar.grid(column = 2, row = 0, padx = 5, pady = 10)
            
            
            
            # Add a button for doing a dry run
            self.cut_dry_run_button_box =  Pmw.ButtonBox(self.cluster_page_cutoff.interior(), padx=0, pady=1,orient='vertical')
            
#            self.cut_dry_run_button_box.pack(side=LEFT, anchor = 'w')
            self.cut_dry_run_button_box.grid(column=0, row=1, columnspan = 2)
            
            self.cut_dry_run_button_box.add('Count Clusters', command = self.count_clusters_button_pressed)
            
            self.cut_cluster_count_field = Pmw.EntryField(self.cluster_page_cutoff.interior(),
                                     label_text="",
                                     labelpos='w',
                                     command=self.cluster_count_changed
                                     #modifiedcommand=self.contacts_selection_changed
                                     )
            self.cut_cluster_count_field.setvalue(self.cluster_count.get())
#            self.cut_cluster_count_field.pack(side=LEFT, padx = 20, anchor = 'sw')
            self.cut_cluster_count_field.grid(column=0, row=2, columnspan = 2)
            
            #add help text for the cutoff
            self.cut_frame_help_text = tkinter.Label(self.cluster_page_cutoff.interior(),
                                                            pady = 5,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = cutoff_help_text)
#            self.cut_frame_help_text.pack(side='right', pady=1, padx=4)
            self.cut_frame_help_text.grid(column = 3, row = 0, columnspan=4, rowspan=3, padx = 10, pady = 10, sticky=W+E)
            
            
            
            
            ######### check box for grouping singletons ##############
            self.singletons_radio = Pmw.RadioSelect(self.cluster_page_singletons.interior(),
                                             selectmode='single',
                                             buttontype='checkbutton',
                                             labelpos='w',
                                             label_text='Group singleton clusters?',
                                             orient='vertical',
                                             frame_relief='ridge',
                                             command=self.singletons_mode_changed)
            self.singletons_radio.pack(side = LEFT, padx = 10, anchor='w')
    
            self.singletons_radio.add('singletons', text = '')
            if self.singletons.get():
                self.singletons_radio.invoke('singletons')
    
                
            #add help text for the singletons
            self.singletons_radio_help_text = tkinter.Label(self.cluster_page_singletons.interior(),
                                                            pady = 5,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = group_singletons_help_text)
            self.singletons_radio_help_text.pack(side='right', pady=1, padx=4)
            
                        
            ######### check box for computing contacts ##############
            self.contacts_radio = Pmw.RadioSelect(self.cluster_page_contacts.interior(),
                                             selectmode='single',
                                             buttontype='checkbutton',
                                             labelpos='w',
                                             label_text='Compute contacts with selection?\nTip: Add hydrogens to the receptor for best results.',
                                             orient='vertical',
                                             frame_relief='ridge',
                                             command=self.contacts_mode_changed)
            self.contacts_radio.pack(side = LEFT, padx = 10, anchor='w')
    
            self.contacts_radio.add('contacts', text = '')
            if self.contacts.get():
                self.contacts_radio.invoke('contacts')
    
            self.contacts_entry = Pmw.EntryField(self.cluster_page_contacts.interior(),
                                                 label_text="Selection:",
                                                 labelpos='w',
                                                 #command=self.contacts_selection_changed
                                                 modifiedcommand=self.contacts_selection_changed
                                                 )
            self.contacts_entry.setvalue(self.contacts_selection.get())
            self.contacts_entry.pack(side=LEFT,padx = 20, anchor='w')
                    
            self.contacts_radio_help_text = tkinter.Label(self.cluster_page_contacts.interior(),
                                                            pady = 5,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = group_contacts_help_text)
           
            # self.contacts_radio_help_text.pack(side=BOTTOM, pady=1, padx=4)
            
    
                    
            ################# Add the button for creating the clusters ##########################
            self.cluster_button_box = Pmw.ButtonBox(self.cluster_page_cluster.interior(), padx=0, pady=1,orient='vertical')
            self.cluster_button_box.pack(side=LEFT)
            self.cluster_button_box.add('\n\n     Create Clusters     \n\n', command = self.cluster_button_pressed)
            
            #add help text for the cutoff
            self.cluster_button_help_text = tkinter.Label(self.cluster_page_cluster.interior(),
                                                            pady = 5,
                                                            justify=LEFT,
                                                            wraplength = 400,
                                                            background = 'white',
                                                            foreground = 'black',
                                                            text = make_clusters_help_text)
            self.cluster_button_help_text.pack(side='right', pady=1, padx=4)
            
        
        #######################################################################################
        #############################  Add an About tab ####################################### 
        #######################################################################################
        if True:
            self.about_page = self.notebook.add('About')
            
            self.about_page_group = Pmw.Group(self.about_page, tag_text='About cluster_mols')
            self.about_page_group.pack(fill = 'both', expand = 0, padx = 10, pady = 5)
    
            self.about_text_field = tkinter.Label(self.about_page_group.interior(),
                                             text = about_text,
                                             background = 'white',
                                             foreground = 'black',
                                             justify = LEFT,
                                             )
            self.about_text_field.pack(expand = 0, fill = 'both', padx = 4, pady = 4)

        
        
        
        
        
        
        
        
        #### finish up with some stuff ####
        self.notebook.setnaturalsize()
        
            
        #show the ligands dialog first
        self.ligand_select_dialog.show()
        

        
        
    
    def get_pickle_file_name(self):
        if self.ligand_file:
            return os.path.splitext(os.path.basename(self.ligand_file))[0] + '_pickle.p'
     
    def button_pressed(self, result):
        if DEBUG > 2:
            print('button_pressed result', result)
        if hasattr(result,'keycode'):
            if result.keycode == 36:
                print('keycode:', result.keycode)
        elif result == 'Ok' or result == 'Exit' or result == None:
            self.dialog.withdraw()

    def auto_compute_and_cluster(self, infile, outfile, cpus, cutoff = 2.0, singletons = True, sort = 'size'):
        '''
        A convience function for automatically running the compute_sim_matrix
        and cluster_and_extract functions one after another.
        
        Arguments:
        infile -- ligand file name
        outfile -- picklefile name
        cpus -- how many cpus to run on
        cutoff -- how high to cut the hierarchical clustering
        singletons -- boolean for whether to group singleton clusters
        sort -- string for what to sort clusters by. 'size' or valid SD tag
        
        Returns:
        outfiles -- output files
        '''

        self.all_sims = compute_sim_matrix(infile, outfile = outfile, cpus = cpus)
        #if it's empty, there was an error, so don't continue
        if len(self.all_sims) != 0:
            self.outfiles = cluster_and_extract(self.all_sims, infile, cutoff = cutoff, singletons = singletons, sort = sort)
                
        if self.outfiles != None:
            self.load_clusters(self.contacts.get(), self.contacts_selection.get())
    #        return outfiles

    def ligand_button_pressed(self, result):
        if DEBUG > 2:
            print('button_pressed result', result)
        if hasattr(result,'keycode'):
            if result.keycode == 36:
                print('keycode:', result.keycode)
        elif result == 'Ok':
            self.ligand_select_dialog.withdraw()
            #show the main window
            self.dialog.show()
            
            if self.ligand_file:
                if SWITCH_PAGE_ON_CALC_SIMS:
                    self.notebook.selectpage(1)
                if AUTORUN_CLUSTERING:
                    #automaticially start the similarity calculations then run the clustering
                    Thread(target = self.auto_compute_and_cluster, args = [ self.ligand_file], kwargs={'outfile': self.picklefile , 'cpus': self.cpus.get(), 'cutoff': self.cutoff.get(), 'singletons': self.singletons.get(), 'sort': self.sort_list.get() }).start()
                
                #automaticially start the similarity calculations
    #            self.comp_sims_button_pressed()
                #Automaticially run the clustering
    #            self.cluster_button_pressed()
                
        elif result == 'Cancel' or result == None:
            self.ligand_select_dialog.withdraw()
            self.dialog.withdraw()
            
    def comp_sims_button_pressed(self):
        global RUNNING
        global REDO
        
        if DEBUG > 3:
            print('RUNNING:', RUNNING)
            print('REDO:', REDO)
            print('len(self.all_sims):', len(self.all_sims))
        #check to see if you are running already (in case the user double clicked
        if RUNNING:
            #if it says that it's running, but it's done, reset the marker
            if len(self.all_sims) == 0:
                print('The similarity calculations are already running! Wait until they are finished!')
                return
            else:
                RUNNING = False
                
        else:
            RUNNING = True
        
        
        print('self.ligand_file:', self.ligand_file)
        print('self.picklefile:', self.picklefile)
        
        if not self.ligand_file or not self.picklefile:
            print('Error! You need to select a ligand file first!')
            RUNNING = False
            return
        
        if len(self.all_sims) == 0 or REDO:
            if DEBUG > 2:
                print('Running on', self.cpus.get(), 'CPUs')
                
            # call the computation in the new thread - the results will be saved to a pickle file and be loaded in later
            Thread(target = compute_sim_matrix, args = [ self.ligand_file], kwargs={'outfile': self.picklefile , 'cpus': self.cpus.get()}).start()
            
                    #if you now have a valid ligand file, move the green color to the compute similarities page 
        if self.ligand_file:

            #switch to the Cluster Compounds page automaticially after calculating the simulations
            if SWITCH_PAGE_ON_CALC_SIMS:
                self.notebook.selectpage(1)
            
            #change the color of the compute similarities button back to grey
            compute_sim_button = self.compute_sim_button_box.button(Pmw.END)
            compute_sim_button.config(bg = '#d9d9d9') #grey
            compute_sim_button.config(activebackground = '#ececec') #light grey
            
            #change the color of the Create Clusters button to green
            cluster_button = self.cluster_button_box.button(Pmw.END)
            cluster_button.config(bg = HIGHLIGHT_COLOR)
            cluster_button.config(activebackground = HIGHLIGHT_COLOR_ACTIVE)
            
    def cluster_button_pressed(self, dry_run = False):
        global RUNNING
        global KILL

        print('self.picklefile:', self.picklefile)

        #check to see if the similarity calculation is done
        if not self.ligand_file or not self.picklefile or not os.path.exists(self.picklefile):
            print('Error! File with saved similarities not found!\n(Re)run the similarity calculation if it is not currently running (check console) or wait')
            print(self.picklefile)
            return
        else:
            RUNNING = False
            
            #Load in the similarity matrix from the pickle file
            #sometimes this throws an error. it seems to be when an old version of numpy is installed
            try:
                self.all_sims = pickle.load(open(self.picklefile, 'rb'))
            except:
                print('Error! If you get an error here about "scimath", it may be that your numpy is out of date.')
        
        #change the color of the Create Clusters Button back to grey
        cluster_button = self.cluster_button_box.button(Pmw.END)
        cluster_button.config(bg = '#d9d9d9')
        cluster_button.config(activebackground = '#ececec')
        
        self.outfiles = cluster_and_extract(self.all_sims, self.ligand_file, cutoff = self.cutoff.get(), singletons = self.singletons.get(), sort = self.sort_list.get(), dry_run = dry_run)
            
        if not dry_run:
            self.load_clusters(self.contacts.get(), self.contacts_selection.get())
        else:
            self.cluster_count.set(str(self.outfiles))
            
    
    def count_clusters_button_pressed(self):
        ''' Do a dry run of counting the clusters '''
        #call cluster_button_pressed with dry run 
        self.cluster_button_pressed(dry_run = True)
        self.cut_cluster_count_field.setvalue(str(self.cluster_count.get()))
        
    def cutoff_changed(self, x):
        ''' handle the cutoff being changed'''
        
        incr = 0.05
        old_cutoff = self.cutoff.get()
        
        new_cutoff = old_cutoff + float(x)*incr
        #round it off to 2 digits
        new_cutoff = round(new_cutoff, 2)
        if new_cutoff >= 100.0:
            new_cutoff = 100.0
        elif new_cutoff <= 0.0:
            new_cutoff = 0.0
        if DEBUG > 2:
            print('changing cutoff from:', old_cutoff, 'to:', new_cutoff)
        
        self.cutoff.set(new_cutoff)
        
    def cpus_changed(self, x):
        #change the number of CPUs to be used. Make sure that there are that many
        if 0 < x <= mp.cpu_count():
            self.cpus.set(x)
        elif x < 1:
            self.cpus.set(1)
        elif x > mp.cpu_count():
            self.cpus.set(mp.cpu_count)
            
    def singletons_mode_changed(self, button_name, pressed):
        if pressed:
            self.singletons.set(True)
        else:
            self.singletons.set(False)
    
    def redo_checkbox_changed(self, button_name, pressed):
        global REDO
        if pressed:
            REDO = True
            self.redo.set(True)
        else:
            REDO = False
            self.redo.set(False)
        if DEBUG > 3:
            print('REDO set to:', REDO)
    
    def contacts_mode_changed(self, button_name, pressed):
        if pressed:
            self.contacts.set(True)
        else:
            self.contacts.set(False)
            
    def contacts_selection_changed(self):
        if DEBUG > 4:
            print('Contact selection changed to:', self.contacts_entry.getvalue())
        self.contacts_selection.set(self.contacts_entry.getvalue())          
                        
    def populate_sort_list(self):
        
        #clear any thing that is in the list already
        self.sort_list.clear()
        
        self.sort_list.insert('end', 'None')
        self.sort_list.insert('end', 'size')
        
        if self.ligand_file:
            if DEBUG > 2:
                print('populate_sort_list: ligand_file:', self.ligand_file)
            headers = sdf_header(self.ligand_file)
            
            for header in headers:
                if header not in ['Rank']:
                    self.sort_list.insert('end', header)
            
        self.sort_list.selectitem('size')
        
        #dkoes - I prefer defaulting to minimizedAffinity
        if default_settings['sort'] in self.sort_list.get(0, END):
            self.sort_list.selectitem(default_settings['sort'])
    
    def populate_ligand_select_list(self):
        ''' Go thourgh the loaded objects in PyMOL and add them to the selected list. '''
        #get the loaded objects
        loaded_objects = [ name for name in cmd.get_names() if '_cluster_' not in name ]
         
        self.ligand_select_object_combo_box.clear()
        self.select_ligand_combo_box.clear()
        
        for ob in loaded_objects:
            self.ligand_select_object_combo_box.insert('end', ob)
            self.select_ligand_combo_box.insert('end', ob)
        
    def selectionCommand(self, value):
        sels = self.sort_list.getcurselection()
        
    def load_clusters(self, showcontacts=False, showcontacts_selection=''):
        if DEBUG > 3:
            print('loading clusters')
        if self.ligand_file and self.outfiles:
            group_name = os.path.splitext(os.path.basename(self.ligand_file))[0] + '_clusters'
            #cut off the S from clusters
            cluster_pref = group_name[:-1]
            
            #if the group is already loaded, delete it
            if group_name in cmd.get_names():
                cmd.delete(group_name)
            
            #if there are problems in the future:
            #In case the grouping got messed up, maybe try to delete everything, don't delete the selected compounds, though 
            
            #load in the files
            names = []
            for i,outfile in enumerate(self.outfiles):
                if DEBUG > 1:
                    print('loading cluster:', i)
                #load it the molecules as discrete objects so objectfocus keys work properly
                cmd.load(outfile, discrete = 1, zoom = False)
                if showcontacts: #create the contacts and group appropriately
                    if DEBUG > 2:
                        print("About to show contacts")

                    #outfile is like: /tmp/XXX_cluster_0.sdf
                    ligname = os.path.splitext(os.path.basename(outfile))[0]

                    if DEBUG > 3:
                        print('Sel1:', showcontacts_selection)
                        print('Sel2:', ligname)
                    
                    #run show contacts and check for a return value
                    ret_val = show_contacts(showcontacts_selection, ligname, ligname + '_contacts')
                    
                    if not ret_val:
                        print('Error is show_contacts for cluster', i,'!')
                        return False
                    
                    #rename ligands so we can use the name for the grouping of sturctures and contacts
                    cmd.set_name(ligname, ligname + '_ligands')
                    cmd.group(ligname, '%s_ligands %s_contacts' % (ligname, ligname))
                    cmd.disable(ligname)
                    names.append(ligname)
            
            #create the master group
            if showcontacts:
                #group them - need to specify names exactly to avoid weirdness 
                cmd.group(group_name, ' '.join(names)) #, action = 'open'
            else:
                #get the pymol names of the outfiles
                names = [ os.path.splitext(os.path.basename(outfile))[0] for outfile in self.outfiles ] 
                cmd.group(group_name, ' '.join(names)) #, action = 'open'
                cmd.disable(group_name)
            
            #Show the clusters as sticks
            cmd.show('sticks', group_name)
            
            #disable all of them except the first one
            for obj in names[1:]:
                cmd.disable(obj)
            
            cmd.enable(names[0])
            cmd.zoom(names[0])
            if DEBUG > 1:
                print('Found', len(self.outfiles), 'clusters')
                
            if SHOW_PLOT:
                plt.show()
            
            if DEBUG > 0:
                print('Done')
                
    def load_ligands_dialog(self, app):
        return tkinter.filedialog.askopenfilename(title = 'Open ligand file...',
                                            filetypes = [('SDF files', '.sdf'), ('compressed SDF files', '.sdf.gz'), ('All Files', '*.*')] )
     
    def reload_ligands_dialog_pressed(self):
        ''' reopen the load ligand dialog box when the button is clicked. '''
        #save the ligand file name so it doesn't get cleared if the user goes to select a new ligand and then hits cancel
        old_ligand_file = self.ligand_file
        self.ligand_file = self.load_ligands_dialog(self.app)
        self.ligand_select_dialog.show()
        if not self.ligand_file:
            self.ligand_file = old_ligand_file
        self.display_ligand_file.set(self.ligand_file)
        
        #update the pickle file name
        self.picklefile = self.get_pickle_file_name()
      
        #populate the list of headers
        self.populate_sort_list()
        
        #if you now have a valid ligand file, move the green color to the compute similarities page 
        if self.ligand_file:
            #change the color of the select ligands button back to gray
            load_ligand_button = self.load_ligand_button_box.button(Pmw.END)
            load_ligand_button.config(bg = '#d9d9d9')
            load_ligand_button.config(activebackground = '#ececec')
            
            #change the color of the compute similarities button
            compute_sim_button = self.compute_sim_button_box.button(Pmw.END)
            compute_sim_button.config(bg = HIGHLIGHT_COLOR)
            compute_sim_button.config(activebackground = HIGHLIGHT_COLOR_ACTIVE)
            
    def select_loaded_object_command(self, value):
        if DEBUG > 4:
            print('value:', value)
        
        if not value in cmd.get_names():
            print('Error! Can not find', value, 'open in PyMOL! Maybe it was deleted or renamed?')
            print('Refreshing the list...')
            #add the current objects to the list
            self.populate_ligand_select_list()
            return
             
        #save it to an sdf file
        saved_file = tempfile.gettempdir() + os.sep + value + '.sdf'
        cmd.save(saved_file, value, state = 0) #save all
        
        #set the ligand file
        self.ligand_file = saved_file
        
        #display the name in the text field
        self.display_ligand_file.set(value)
        
        #update the pickle file name
        self.picklefile = self.get_pickle_file_name()
      
        #populate the list of headers
        self.populate_sort_list()
        
        return value
    
    def cluster_count_changed(self):
        ''' handle the number of clusters changed '''
        print('Not Implemented! cluster_count_changed')
    
def __init__(self):
    self.menuBar.addmenuitem('Plugin', 'command', 'Cluster Molecules Client', label = 'Cluster Molecules', command = lambda s=self : Cluster_Mols(s))  
        
if __name__ == '__main__':
    if PROFILE:
        cProfile.run('main()', 'profile_stats')
        p = pstats.Stats('profile_stats')
        p.strip_dirs().sort_stats('cumulative').print_stats()
    else:
        main()
        if SHOW_PLOT:
            plt.show()



# s.send('Hello, world')
# data = s.recv(1024)
# s.close()
# print 'Received', repr(data)
