#!/usr/bin/python
 
 
#quickly cluster molecules in an input sdf file and display them in pymol
#Server side code for ClusterMols
#Matthew Baumgartner
#5-6-13
#mpb21@pitt.edu

#Copyright 2016 Matthew Baumgartner


from __future__ import with_statement

import sys
import os
import subprocess as sp
import tempfile
import re
import cPickle as pickle
import itertools
import logging

import cgi
# import cgitb
# cgitb.enable(display=0, logdir="/home/baumgartner/cluster_mols/logs/")
# cgitb.enable()
#make sure the cgi user can see the fastcluster install dir
# /usr/local/lib/python2.7/dist-packages/
sys.path.insert(0, "/usr/local/lib/python2.7/dist-packages/")

#we need a directory for the program to extract the python eggs to
EGG_DIR = '/tmp/eggs/'
if not os.path.exists(EGG_DIR):
    os.mkdir(EGG_DIR)
os.environ['PYTHON_EGG_CACHE'] = EGG_DIR


try:
    import chemfp
except ImportError:
    sys.stderr.write("\n\n'chemfp' package not found! Install using 'sudo pip install chemfp'\n\n")
    sys.exit()
#We don't actually need openbabel directly, but chemfp does, so let's make sure we can get to it now.
try:
    import openbabel as ob
except ImportError:
    sys.stderr.write("\n\n'openbabel' package not found! Install from openbabel.org (Make sure to compile python bindings)\n\n")
    sys.exit()
    
# import gzip
import tarfile

try:
    import numpy as np #There have been problems that may occur if your numpy installation is older than 0.15, but I haven't confirmed it
except ImportError:
    sys.stderr.write("\n\n'numpy' package not found! Install using 'sudo pip install numpy'\n\n")
    sys.exit()
    
    
import multiprocessing as mp

from threading import Thread
try:
    import scipy
except ImportError:
    sys.stderr.write("\n\n'scipy' package not found! Install using 'sudo pip install scipy'\n\n")
    sys.exit()
    
    
import scipy.cluster
try:
    import fastcluster
except ImportError:
    sys.stderr.write('\n\nError: "fastcluster" package missing! Run "sudo easy_install fastcluster" to install.\n\n')

from time import sleep
import time
import shutil
import cStringIO #for trashing the stdout
import hashlib



#Amount of output to print out - higher is more (default = 2)
# Values above 3 can make pymol unstable for some reason
#this can be set through the command line with the '-v' option
DEBUG = 3

#force recomputing everything - i.e. ignore pickled data (Default = False)
REDO = False

#Do a runtime profile of the script
PROFILE = False
if PROFILE:
    import cProfile
    import pstats


#executable for babel. Hopefully it can be just 'babel', but if not try to figure out the path a bit
BABEL = None


POSSIBLE_BABEL_LOCS = ['babel', '/usr/bin/babel', '/opt/local/bin/babel', '/usr/local/bin/babel']

#make sure you can reach babel
for bab in POSSIBLE_BABEL_LOCS:
    try:
        p = sp.Popen([bab, '--help'], stdout = sp.PIPE, stderr = sp.PIPE)
        out,err = p.communicate()
#         logging.info('Found babel at ' + str(bab))
        BABEL = bab
        break
    except OSError:
        #since you can't find it, you are probably on OSX and you are having hard to pin down PATH issues. So try to figure out where it is with an absolute path
#         logging.error("Can't find babel at: " + str(bab))
        sys.stderr.write("Can't find babel!\n")

if BABEL == None:
    sys.stdout.write("\n\nError! Can not find the 'babel' command line tool! It must be in your $PATH.\n")
    sys.stdout.write('If you do not have openbabel, get it from here:\n')
    sys.stdout.write('www.openbabel.org\n\n')
    sys.exit()


#import the client file for all of the actual commands so they aren't duplicated in the code
# cluster_mols_client.RUN_REMOTELY = False
from cluster_mols import compute_sim_matrix
from cluster_mols import cluster_and_extract

#There is probably a better way to do this, but for now, manually supply the fake client with the imports that the actual client probably doesn't have
import cluster_mols
cluster_mols.BABEL = BABEL
cluster_mols.chemfp = chemfp
cluster_mols.np = np
cluster_mols.scipy = scipy
cluster_mols.fastcluster = fastcluster
cluster_mols.DEBUG = 0 #Shut off all talking from the client as it messes with the returned HTML headers


#Boolean for whether the similarities are being calculated now or not
global RUNNING
RUNNING = False

#Boolean that you can set during the runtime to kill a currently running simulation
#set by typing the following into the pymol command line: cluster_mols. KILL = True
#2-17-14 This may or may not work.
KILL = False



#Version string
VERSION = 'v2.1.1b-server'

#Release date - MM-DD-YYYY
RELEASE_DATE = '12-9-2015' 

#boolean for whether to autorun the calculations after choosing a ligand
AUTORUN_CLUSTERING = True




def is_pickle_file(infile):
    '''
    Determine if a file is a python pickle file.
    
    Arguments:
    infile -- file to be tested
    
    Returns:
    boolean
    '''
    
    try:
        pickle.load(open(infile))
    except:
        logging.error(str(infile) + ' is not a picklefile')
        return False
    
    return True
    
    

def main():
    #     print 'welcome!'
    logging.basicConfig(filename = '/home/baumgartner/cluster_mols/webserver.log', filemode = 'a', format='%(levelname)s %(asctime)s  %(message)s', level = logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info("\n\nClusterMols Webserver starting up at: " + time.strftime('%c'))
#     print "Content-Type: text/html"     # HTML is following
#     print                               # blank line, end of headers

    #message to pass back to the client
    message = ''

    #a flag that provides validation at each step. if there is a problem, generate a message and skip forward to sending it back
    OK_TO_CONTINUE = True
    
    #parse the post from the client
    form = cgi.FieldStorage(fp=sys.stdin)
#     cgi.print_form(form)
    
    workdir = tempfile.mkdtemp()
    #give it a suffix of .sdf even if it isn't
    infile = tempfile.NamedTemporaryFile('wrb',  dir = workdir, suffix = '.sdf', delete = False)
    datafile = tempfile.NamedTemporaryFile('wb',  dir = workdir, suffix = '.p', delete = False)
    outfile = os.path.splitext(infile.name)[0] + '_pickle.p'
    #Receive the input file, it could be an sdf file or a pickle file
    file_obj = open(datafile.name, 'wb')
    
    
    
    if OK_TO_CONTINUE:
        #write out the sdf file
        input_sd_data = form.getfirst('infile', '')
        #count how many molecules there are
        num_comps = input_sd_data.count('$$$$')
        logging.info("Received " + str(num_comps) + ' compounds') 
        if num_comps == 0:
            message = "Error! There are no compounds in the input!\n"
            OK_TO_CONTINUE = False
        infile.write(input_sd_data)
        infile.flush()
    
        logging.info(infile.name + ' written')
        
    
        
    #get the args
    if OK_TO_CONTINUE:
        cpus = form.getfirst('cpus', '') #ignored
        cutoff = form.getfirst('cutoff', '')
        singletons = form.getfirst('singletons', '')
        sort = form.getfirst('sort', '')
        dry_run = form.getfirst('dry_run', '')
#         picklefile = form.getfirst('picklefile', '')
        logging.debug('starting to cast')
        #Type cast everything
        try:
            cutoff = float(cutoff)
        except:
            message += "Error! Invalid 'cutoff' parameter provided.\n"
            logging.error("Error! Unable to cast cutoff to float")
            OK_TO_CONTINUE = False
            
        try:
            singletons = singletons == "True"
        except:
            message += "Error! Invalid 'singletons' parameter provided.\n"
            logging.error("Error! Unable to cast singletons to boolean")
            OK_TO_CONTINUE = False
            
        try:
            dry_run = dry_run == "True"
        except:
            message += "Error! Invalid 'dry_run' parameter provided.\n"
            logging.error("Error! Unable to cast dry_run to boolean")
            OK_TO_CONTINUE = False
        
        if not isinstance(sort, str) or sort == '':
            message += "Error! Invalid 'sort' parameter provided.\n"
            logging.error("Error! No sort parameter provided")
            OK_TO_CONTINUE = False
        
        
    logging.debug('cpus: ' + str(cpus))
    logging.debug('cutoff: ' + str(cutoff))
    logging.debug('singletons: ' + str(singletons))
    logging.debug('sort: ' + str(sort))
    logging.debug('dry_run: ' + str(dry_run))
    logging.debug("OK_TO_CONTINUE: " + str(OK_TO_CONTINUE))    
    
     
    logging.debug("receiving file")


    #take the hash of the input compounds and see if you have saved results for them 
    
    if OK_TO_CONTINUE:
        all_sims = None
    
        #get a hash of the input data
        hash = hashlib.sha256(input_sd_data).hexdigest()
        
        #See if you have a pickle file with that name
        pickle_dir = '/tmp/pickles/' 
        if not os.path.exists(pickle_dir):
            os.mkdir(pickle_dir)
        
        outfile = pickle_dir + hash + '.p'
        if os.path.exists(outfile):
            logging.info("Loading previously saved results " + outfile)
            all_sims = pickle.load(open(outfile))
        else:
            picklefile = ''
            all_sims = None
#     
#         #see if the pickle file is there, if so write it to a file
#         if picklefile:
#             
#             all_sims = pickle.loads(picklefile)
#             
#             #do a bit of validation on the pickle file, if anything fails, ignore it and recompute
#             try:
#                 assert all_sims != []
#                 logging.debug("all_sims not empty")
#                 assert isinstance(all_sims, np.ndarray)
#                 logging.debug("all_sims is np.array")
#                 assert np.shape(all_sims) == (num_comps, num_comps) #make sure that it's the right size
#                 logging.debug("all_sims is the right shape")
#                 pickle.dump(all_sims, open(outfile, 'wb'), protocol = -1)
#                 logging.info('pickle file saved to: ' + outfile)
#             except Exception as exc:
#                 logging.warn("Invalid pickle data received, ignoring. Error: " + exc.__str__())
#                 picklefile = ''
#                 all_sims = None
#             
    
    
    
    
    if OK_TO_CONTINUE:
        if all_sims == None:
            assert os.path.exists(infile.name), 'Error! File not found: ' + str(infile.name) + '\n'
            logging.info('computing similarity matrix')
            
            save_stdout = sys.stdout #supress the stdout from the client code
            sys.stdout = cStringIO.StringIO()
            all_sims = compute_sim_matrix(infile.name, outfile, cpus = mp.cpu_count())    
            sys.stdout = save_stdout #restore it
        
    #         logging.info('all_sims: ' + str(all_sims))
        if all_sims != None:
            logging.info('clusting and extracting')
            save_stdout = sys.stdout #supress the stdout from the client code
            sys.stdout = cStringIO.StringIO()
            outfiles = cluster_and_extract(all_sims, infile.name, cutoff = cutoff, singletons = singletons, sort = sort, dry_run = dry_run)
            sys.stdout = save_stdout #restore it
            logging.info('clusting and extracting done')
        
            if dry_run:
                #if dry_run is true, outfile will be a number 
                logging.info('Dry Run: Will produce ' + str(outfiles) + ' clusters')
                message += 'Dry Run: Will produce ' + str(outfiles) + ' clusters'
            else:
                if len(outfiles) == 1:
                    message += 'Warning! Only one cluster was found! Lower the cutoff (-c) to get more.'
                    logging.warn('Warning! Only one cluster was found! Lower the cutoff (-c) to get more.')
        
   
    
    ############## Send everything back  #############
    
    
    logging.info("moving file to tmpdir")
    #compress all of the output files into one gzip file
    tmpdir = tempfile.mkdtemp()
    if not dry_run:
        for outf in outfiles:
            shutil.move(outf, tmpdir)
    
    wd = os.getcwd()
    
    #move to the tmpdir and then go up one (should be in /tmp/)
    os.chdir(tmpdir)
    os.chdir('..')
    
    
    if dry_run:
        fout = open(tmpdir + '/dry_run.txt', 'wb')
        fout.write(str(outfiles)) #cluster counts
        fout.close()
    
    #if there are any message to pass back write a file
    if message:
        message_file = tmpdir + '/messages.txt'
        fout = open(tmpdir + '/messages.txt', 'wb')
        fout.write(message)
        logging.info("Sending messages: " + message)
        fout.close()
    
    
    tmptar = tempfile.NamedTemporaryFile(suffix='.tar.gz', delete = False)
    logging.info("Tar-ing things up! " + str(tmptar.name))
    tar = tarfile.open(fileobj = tmptar, mode = 'w:gz')
    tar.add(tmpdir)
    tar.add(outfile) #add the pickle file
    if message:
        tar.add(message_file)
    tar.close()
    tmptar.close()
    
    logging.debug('tar.name: ' + tar.name)
    
    os.chdir(wd)
    
    #now send the results back
    
    

    logging.info("sending file to client")
    tar_basename = os.path.basename(tmptar.name)
    print "Content-Type:application/x-tar; name=\"{0}\"".format(tar_basename) 
    print "Content-Disposition: attachment; filename=\"{0}\"".format(tar_basename) 
    print                               # blank line, end of headers
#     print '<H1>starting</H1>'

    
    #HTML headers
#     print "Content-Type: text/html"     # HTML is following

    #create a file object of the file to send
    file_obj = open(tmptar.name, 'rb')
    while True:
        data = file_obj.read(4096)
        sys.stdout.write(data)
        if not data:
            break

    sys.stdout.flush()



    logging.info("Done. File sent to client")




if __name__ == '__main__':
    try:
        main()
    except Exception as exc:
#         print "The server had a problem!"
        logging.error("The Server had an error! " + exc.__str__())





